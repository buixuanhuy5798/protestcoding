//
//  ResponseViewPointStudent.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/28/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct ResponseViewPointStudent: Codable {
    var cd:Int?
    var data:DataViewPointStudent?
    var msg:String?
}
