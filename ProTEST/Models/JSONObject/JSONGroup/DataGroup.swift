//
//  DataGroup.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/3/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct DataGroup: Codable {
    var arr: [ArrGroup]?
    var len: String?
}
