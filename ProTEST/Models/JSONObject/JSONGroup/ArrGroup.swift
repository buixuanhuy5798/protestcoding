//
//  ArrGroup.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/3/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct ArrGroup: Codable {
    var gid: Int?
    var tname: String?
    var tuser: String?
    var gdate: String?
    var status: String?
    var gmember: String?
    var gname: String?
}
