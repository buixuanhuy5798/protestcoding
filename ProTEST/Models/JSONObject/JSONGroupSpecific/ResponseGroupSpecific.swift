//
//  JSONGroupSpecific.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct ResponseGroupSpecific: Codable {
    var cd: Int?
    var data: DataGroupSpecific?
    var msg: String?
}
