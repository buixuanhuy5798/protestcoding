//
//  DataGroupSpecific.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct DataGroupSpecific: Codable {
    var arr: [ArrGroupSpecific]?
    var len: String?
    var tname: String?
    var tuser: String?
    var control: Bool?
    var gname: String?
    var gid:Int?
}
