//
//  ResponseSearch.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/2/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct ResponseSearch: Codable {
    var cd: Int?
    var data: DataSearch?
    var msg: String?
}
