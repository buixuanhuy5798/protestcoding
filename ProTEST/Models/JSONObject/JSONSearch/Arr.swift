//
//  Arr.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/2/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct Arr: Codable {
    var cmnd: String?
    var item: Int?
    var name: String?
    var phone: String?
    var type: String?
    var gname: String?
    var tuser: String?
    var gdate: String?
    var tname: String?
    var status: String?
    var itemhuman: String?
}
