//
//  Member.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/11/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct Member {
    var name:String!
    var user: String!
    init(_ name:String,_ user:String) {
        self.name = name
        self.user = user
    }
}
