//
//  ArrContest.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct ArrContest: Codable {
    var created:String?
    var eid:String?
    var name:String?
    var publish:Bool?
    var qlen:String?
    var timeEnd:String?
    var timeStart:String?
    var tuser:String?
    var type:String?
    
}
