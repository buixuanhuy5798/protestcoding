//
//  File.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct ResponseContest: Codable {
    var cd:Int?
    var data: DataContest?
    var msg:String?
}
