//
//  DataLogin.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/3/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct DataLogin: Codable {
    var cmnd: String?
    var name: String?
    var phone: String?
    var elen:String?
    init(_ cmnd:String,_ name:String,_ phone:String) {
        self.cmnd = cmnd
        self.name = name
        self.phone = phone
    }
}
