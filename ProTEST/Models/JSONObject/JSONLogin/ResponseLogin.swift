//
//  ResponseLogin.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/3/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation

struct ResponseLogin: Codable {
    var msg: String?
    var cd: Int?
    var data: DataLogin?
}
