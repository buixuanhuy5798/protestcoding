//
//  ResponseExam.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct ResponseExam : Codable {
    var cd:Int?
    var data: DataExam?
    var msg:String?
}
