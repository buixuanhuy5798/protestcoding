//
//  DataExam.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
struct DataExam: Codable {
    var aarr: [String]?
    var alen: String?
    var arrGourpAcc: [Int]?
    var created: String?
    var eid:String?
    var lenGroupAcc: Int?
    var name:String?
    var publish:Bool?
    var qarr:[ArrQuestion]?
    var qlen: String?
    var timeEnd:String?
    var timeStart:String?
    var tname:String?
    var tuser:String?
    var type:String?
    

}
