//
//  CellAnswerForStudent.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/20/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellAnswerForStudent: UITableViewCell {

    @IBOutlet weak var PickAnswer: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setChoice(_ answer: String?, _ check: Bool?) {
        self.PickAnswer.text = answer
        if check! {
            btnCheck.setImage(UIImage(named: "check"), for: .normal)
        }
        else {
            btnCheck.setImage(UIImage(named: "noncheck"), for: .normal)
        }
    }
}
