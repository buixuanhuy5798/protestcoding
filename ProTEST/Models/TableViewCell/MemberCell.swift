//
//  MemberCell.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/11/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class MemberCell: UITableViewCell {
    
    
    
    @IBOutlet weak var lblNameMember: UILabel!
    @IBOutlet weak var lblIdMember: UILabel!
    func setMember(_ NameMember:String,_ IDMember: String) {
        self.lblIdMember.text = IDMember
        self.lblNameMember.text = NameMember
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
