//
//  CellQuestion.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellQuestion: UITableViewCell {

    @IBOutlet weak var viewHead: UIView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblQT: UILabel!
    @IBOutlet weak var lblCau: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setQuestion(_ QT:String,_ cau:String){
        view.layer.cornerRadius = 7
        viewHead.layer.cornerRadius = 7
        lblQT.text = QT
        lblCau.text = cau
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
