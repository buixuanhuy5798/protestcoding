//
//  ResultSearchingCell.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/2/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ResultSearchingCell_Teacher: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCmnd: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    func setDuLieu_Teacher(_ name: String, _ phone: String, _ item: String, _ cmnd: String) {
        self.lblName.text = name
        self.lblCmnd.text = cmnd
        self.lblItem.text = item
        self.lblPhone.text = phone
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
