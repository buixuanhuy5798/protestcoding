//
//  GroupCellStudent.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/11/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder
class GroupCellStudent: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var imageGroup: UIImageView!
    @IBOutlet weak var lblNameGroup: UILabel!
    @IBOutlet weak var lblIDGroup: UILabel!
    @IBOutlet weak var lblNumberOfMember: UILabel!
    @IBOutlet weak var lblNameOfTeacher: UILabel!
    @IBOutlet weak var lblStatusGroup: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataForCell( _ name: String?, _ id: Int?, _ numb: String?, _ nameTeacher: String?, _ status: String?) {
        let generator = QRCodeGenerator()
        imageGroup.image = generator.createImage(value: "GR\n" + String(id!) + "\n" + name! + "\n" + numb! + "\n" , size: imageGroup.frame.size)
        lblNameGroup.text = name
        lblIDGroup.text = "\(id!)"
        lblNumberOfMember.text = numb
        lblNameOfTeacher.text = nameTeacher
        if status == "1" {
            lblStatusGroup.textColor = UIColor.green
            lblStatusGroup.text = "Đã tham gia"
        } else if status == "2" {
            lblStatusGroup.textColor = UIColor.gray
            lblStatusGroup.text = "Đang yêu cầu tham gia"
        } else {
            lblStatusGroup.textColor = UIColor.blue
            lblStatusGroup.text = "Được mời tham gia"
        }
    }
    
}
