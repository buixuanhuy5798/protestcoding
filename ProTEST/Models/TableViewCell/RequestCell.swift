//
//  RequestCell.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/13/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {
    
    @IBOutlet weak var btnDeny: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    func setMemberRequest(_ NameMember:String,_ UserMember:String){
        self.lblName.text = NameMember
        self.lblUser.text = UserMember
    
    }
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
