//
//  ResultSearchCell_Group.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/5/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ResultSearchCell_Group: UITableViewCell {
    
    @IBOutlet weak var lbltName: UILabel!
    @IBOutlet weak var lblgDate: UILabel!
    @IBOutlet weak var lbltUser: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var lblgName: UILabel!
    
    func setDuLieu_Group(_ tName: String, _ gDate: String, _ tUser: String, _ item: String, _ gName: String) {
        self.lbltName.text = tName
        self.lblgDate.text = gDate
        self.lbltUser.text = tUser
        self.lblItem.text = item
        self.lblgName.text = gName
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
