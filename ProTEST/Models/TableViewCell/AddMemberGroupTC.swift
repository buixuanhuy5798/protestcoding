//
//  AddMemberGroupTC.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class AddMemberGroupTC: UITableViewCell {
    
    @IBOutlet weak var QRcode: UIImageView!
    @IBOutlet weak var studentName: UILabel!
    @IBOutlet weak var studentID: UILabel!
    
    let task = SocketIOManager.sharedInstance
    var idGroup: String?
    var tUser: String?
    var sUser: String?
    var loading: UIActivityIndicatorView?
    var viewLoading: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataFromServer(_ QRimage: UIImage?, _ studentName: String?, _ studentID: String?) {
        self.QRcode.image = QRimage
        self.studentName.text = studentName
        self.studentID.text = studentID
    }
    
    @IBAction func AddStudent(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        guard let controller = storyboard.instantiateViewController(withIdentifier: "AddMemberController") as? AddMemberController else {
//            return
//        }
//        controller.indexCell = self.btnAdd
        self.task.groupAddOrInviteStudentByTeacher(idGroup, tUser, sUser, loading, viewLoading)
    }
    
}
