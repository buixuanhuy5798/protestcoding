//
//  GroupCell.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/4/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder
class GroupCellTeacher: UITableViewCell {
    
    
    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblGroupMember: UILabel!
    @IBOutlet weak var lblGroupID: UILabel!
    @IBOutlet weak var lblGroupDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func set(_ groupName: String?, _ groupID: Int?, _ groupMember: String?, _ groupDate: String?) {
        let generator = QRCodeGenerator()
        imageQR.image = generator.createImage(value: "GR\n"+String(groupID!) + "\n" + groupName! + "\n" + groupMember! + "\n" + groupDate!, size: imageQR.frame.size)
        lblGroupID.text = String(groupID!)
        lblGroupName.text = groupName
        if (groupDate == "") {
            lblGroupDate.isHidden = true
        }
        else {
            lblGroupDate.text = getDateFromTimeStamp(groupDate!)
        }
        lblGroupMember.text = groupMember
    }
    func getDateFromTimeStamp(_ unix:String) -> String {
        let timeStamp = Double(unix)
        let date = NSDate(timeIntervalSince1970: timeStamp!)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY, hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}
