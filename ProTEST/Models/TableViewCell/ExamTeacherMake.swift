//
//  ExamTeacherMake.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder
class ExamTeacherMake: UITableViewCell {
    
    @IBOutlet weak var btnChitiet: UIButton!
    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblSoCau: UILabel!
    @IBOutlet weak var lblDayend: UILabel!
    @IBOutlet weak var lblDaystart: UILabel!
    @IBOutlet weak var lblTuser: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblexamNgaytao: UILabel!
    @IBOutlet weak var lblexamName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnChitiet.layer.cornerRadius = 7
        viewHeader.layer.cornerRadius = 7
    }
    func setCell(_ examName:String?,_ ngaytao:String?,_ subject:String?,_ socau:String?,_ status:Bool?,_ id:String?,_ tuser:String?,_ daystart:String?,_ dayend:String?){
         let generator = QRCodeGenerator()
        imageQR.image = generator.createImage(value: "EX"+id!, size: imageQR.frame.size)
        lblexamName.text = examName
        lblexamNgaytao.text =  "Ngày tạo: " +  (getDateFromTimeStamp(ngaytao!) ?? "Chưa xét")
        lblSubject.text =  "Môn: " + (subject ?? "Chưa xét")
        lblSoCau.text =  "Số câu: " + (socau ?? "Chưa xét")
        lblID.text =  "ID: " + (id ?? "Chưa xét")
        if (status == true) {
            lblStatus.text = "Public"
            lblStatus.textColor = #colorLiteral(red: 0.2517724633, green: 0.7513584495, blue: 0.5189419389, alpha: 1)
        }
        else {
            lblStatus.text = "Private"
            lblStatus.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
        lblTuser.text = "Người tạo: " + (tuser ?? "Chưa xét")
        if (dayend == ""){
            lblDayend.text = "Kết thúc: Chưa xét"
        } else {
            lblDayend.text = "Kểt thúc: " + (getDateFromTimeStamp(dayend!))
        }
        if (daystart == ""){
            lblDaystart.text = "Bắt đầu: Chưa xét"
        } else {
            lblDaystart.text = "Bẳt đầu: " + (getDateFromTimeStamp(daystart!))
        }
       
        
    }
    func getDateFromTimeStamp(_ unix:String) -> String {
        let timeStamp = Double(unix)
        let date = NSDate(timeIntervalSince1970: timeStamp!)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
