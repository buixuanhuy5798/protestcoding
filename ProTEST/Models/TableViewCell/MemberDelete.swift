//
//  MemberDelete.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class MemberDelete: UITableViewCell {
    
    
    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var lblUser: UILabel!
   
    @IBOutlet weak var btnCheckOL: UIButton!
    var check = false
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func setMember(_ NameMember:String,_ UserMember: String) {
        self.lblMemberName.text = NameMember
        self.lblUser.text = UserMember
    }
    
}
