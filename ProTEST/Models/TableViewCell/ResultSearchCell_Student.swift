//
//  ResultSearchCell_Student.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/5/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ResultSearchCell_Student: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    
    func setDuLieu_Student(_ name: String, _ phone: String, _ item: String) {
        self.lblName.text = name
        self.lblPhone.text = phone
        self.lblItem.text = item
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
