//
//  CellAnswer.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/19/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellAnswer: UITableViewCell {
    
    @IBOutlet weak var lblAnswer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setAnswer(_ Answer:String){
        var x = Int(Answer)
        var y = String(x!+1)
        lblAnswer.text = "Đáp án: " + y
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
