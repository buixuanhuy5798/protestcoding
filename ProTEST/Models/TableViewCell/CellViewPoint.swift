//
//  CellViewPoint.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/28/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellViewPoint: UITableViewCell {

    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var lblUser: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCover.layer.cornerRadius = 10
    }
    func set(_ user:String,_ point:String){
        lblUser.text = user
        lblPoint.text = point
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
