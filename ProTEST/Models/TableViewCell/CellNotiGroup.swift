//
//  CellNotiGroup.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/22/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellNotiGroup: UITableViewCell {

    @IBOutlet weak var lblNoti: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(_ id:String,_ status: String){
        if (status == "2") {
            lblNoti.text = id + " đã gửi yêu cầu tham gia nhóm "
            lblNoti.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        }
        else if (status == "3") {
            lblNoti.text = "Đã gửi yêu cầu tham gia tới " + id
            lblNoti.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }
        else if (status == "4"){
            lblNoti.text = id + " đã từ chối lời mời"
            lblNoti.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        else {
            lblNoti.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            lblNoti.text = id + " đã bị từ chối yêu cầu tham gia"
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
