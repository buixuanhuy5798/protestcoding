//
//  CellChoice.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/19/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class CellChoice: UITableViewCell {

    @IBOutlet weak var lblChoie: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setChoice(_ number:Int,_ choice:String){
        lblChoie.text = String(number) + " - " + choice
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
