//
//  SocketIOManager.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 10/24/18.
//  Copyright © 2018 huy. All rights reserved.
//

import Foundation
import SocketIO
import KeychainSwift

class SocketIOManager : NSObject {
    
    // MARK: - Variables
    var manager: SocketManager = SocketManager(socketURL: NSURL(string: "http://35.240.184.30:3005/")! as URL)
    var socket: SocketIOClient?
    let keychain = KeychainSwift()
    
    // MARK: - Singletion
    static var sharedInstance = SocketIOManager()
    static var firstEmit = true
    static var firstChange = true
    static var firstChange2 = true
    
    // MARK: - Functions
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    // MARK: Connect
    func establishConnection(onConnectedEvent: @escaping () -> Void) {
        if let soc = socket {
            soc.connect()
            if SocketIOManager.firstEmit {
                soc.once("connect") { (data, ack) in
                    onConnectedEvent()
                }
                SocketIOManager.firstEmit = false
            } else {
                onConnectedEvent()
            }
        } else {
            print("Not connect")
        }
    }
    
    // MARK: Disconnect
    func closeConnection() {
        if let soc = socket {
            soc.disconnect()
            
        }
    }
    
    // MARK: Dang nhap
    func DangNhap(_ user: String? ,_ pass: String?) {
        let a = ["user": user,"pass": pass]
        socket?.emit("login", a)
    }
    
    func DNThanhCong(_ loading: UIActivityIndicatorView?) {
        socket?.once("rlogin", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0]
            var rLogin: ResponseLogin?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rLogin = try JSONDecoder().decode(ResponseLogin.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            loading!.stopAnimating()
            loading!.isHidden = true
            if let check = rLogin {
                if check.cd! == 1 {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LoginNoti"), object: nil, userInfo: ["message": check.msg!])
                    self.closeConnection()
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LoginSuccess"), object: nil, userInfo: ["message": check])
                }
            }
        })
    }
    
    // MARK: Dang ki
    func DangKy(_ user: String?, _ pass: String?, _ phone: String?, _ name: String?, _ cmnd: String?, _ type: Int?) {
        if type == 0 { //SINHVIEN
            let a = ["user": user,"pass": pass,"phone": phone,"name": name]
            socket?.emit("regs", a)
        }
        else { //GIANGVIEN
            let a = ["user": user,"pass": pass,"phone": phone,"name": name,"cmnd": cmnd]
            socket?.emit("regt", a)
        }
    }
    
    func DKThanhCong(_ loading: UIActivityIndicatorView?) {
        socket?.once("rreg", callback: { (data, ack) in
            let temp1 = data as NSArray
            let temp2 = temp1[0]
            let temp3 = temp2 as! NSDictionary
            loading!.stopAnimating()
            loading!.isHidden = true
            if (temp3.value(forKey: "cd") as! Int) == 1 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RegisterNoti"), object: nil, userInfo: ["message": temp3.value(forKey: "msg") as! String])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "RegisterSuccess"), object: nil, userInfo: ["message": ""])
            }
            self.closeConnection()
        })
    }
    
    // MARK: Tim kiem
    func pushSearchText(_ utype: String, _ info: String) {
        let up = ["utype": utype, "infosearch": info]
        socket?.emit("searchInfo", up)
    }
    
    func getSearchData(_ loading: UIActivityIndicatorView, _ view1: UIView?, _ view2: UIView?) {
        socket?.once("rsearchInfo", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0]
            var rSearch: ResponseSearch?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rSearch = try JSONDecoder().decode(ResponseSearch.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            loading.stopAnimating()
            loading.isHidden = true
            view1?.isHidden = true
            view2?.isHidden = true
            if let check = rSearch {
                if check.cd! == 1 {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "SearchNoti"), object: nil, userInfo: ["message": check.msg!])
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "SearchResult"), object: nil, userInfo: ["message": check])
                }
            }
        })
    }
    
    // MARK: Lay thong tin tat ca nhom giang vien quan li
    func getInfoAllGroupTeacherManage(_ userteacher: String, _ loading: UIActivityIndicatorView) {
        let up = ["userteacher": userteacher]
        socket?.emit("getInfoAllGroupTeacherManage", up)
        loading.isHidden = false
        loading.startAnimating()
    }
    
    func rgetInfoAllGroupTeacherMange(_ loading: UIActivityIndicatorView) {
        socket?.once("rgetInfoAllGroupTeacherManage", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0]
            loading.stopAnimating()
            loading.isHidden = true
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                let rGroup = try JSONDecoder().decode(ResponseGroup.self, from: dulieu)
                if SocketIOManager.firstChange2 {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoGroupTeacherManage"), object: nil, userInfo: ["message": rGroup.data!])
                    SocketIOManager.firstChange2 = false
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoGroupTeacherManageChange"), object: nil, userInfo: ["message": rGroup.data!])
                }
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    // MARK: Tao nhom
    func createGroup(_ userteacher: String, _ namegroup: String,_ loading: UIActivityIndicatorView,_ viewLoading:UIView) {
        loading.startAnimating()
        loading.isHidden = false
        viewLoading.isHidden = false
        let up = ["namegroup":namegroup,"userteacher":userteacher]
        socket?.emit("createGroupByTeacher",up)
    }
    
    func rcreatGroup(_ loading: UIActivityIndicatorView,_ viewLoading: UIView) {
        socket?.once("rcreateGroupByTeacher", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            if (cd == 0) {
                 NotificationCenter.default.post(name: Notification.Name(rawValue: "CreatGroupSuccess"), object: nil, userInfo: ["message": "0" ])
            }
            else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "CreatGroupSuccess"), object: nil, userInfo: ["message": "1" ])
            }
        })
        loading.isHidden = true
        viewLoading.isHidden = true
    }
    
    // MARK: Xoa nhom
    func deleteGroupByTeacher(_ idgroup: String,_ userteacher: String,_ loading: UIActivityIndicatorView, _ viewLoading: UIView) {
        loading.startAnimating()
        loading.isHidden = false
        viewLoading.isHidden = false
        let up = ["idgroup":idgroup, "userteacher":userteacher]
        socket?.emit("deleteGroupByTeacher", up)
        
    }
    
    func rdeleteGroupByTeacher(_ loading: UIActivityIndicatorView, _ viewLoading: UIView){
        socket?.once("rdeleteGroupByTeacher", callback: { (data, ack) in
            loading.stopAnimating()
            loading.isHidden = true
            viewLoading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            if (cd == 0) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "DeleteGroupSuccess"), object: nil, userInfo: ["message": "0" ])
            }
            else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "DeleteGroupSuccess"), object: nil, userInfo: ["message": "1" ])
            }
        })
    }
    
    // MARK: Lay thong tin nhom
    func getInfoOfGroup(_ idgroup: String,_ loading: UIActivityIndicatorView, _ viewCover: UIView) {
        loading.startAnimating()
        loading.isHidden = false
        viewCover.isHidden = false
        let up = ["idgroup":idgroup]
        socket?.emit("getInfoOfGroup", up)
    }
    
    func rgetInfoOfGroup(_ loading: UIActivityIndicatorView, _ viewCover: UIView){
        socket?.on("rgetInfoOfGroup", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0]
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                let rGroupSpecific = try JSONDecoder().decode(ResponseGroupSpecific.self, from: dulieu)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoGroupSpecific"), object: nil, userInfo: ["message":  rGroupSpecific.data!])
            } catch {
                print(error.localizedDescription)
            }
            
            loading.stopAnimating()
            loading.isHidden = true
            viewCover.isHidden = true
        })
    }
    
    // MARK: Chinh sua ten nhom
    func editGroupByTeacher(_ idgroup:String, _ userteacher:String, _ newname:String, _ loading: UIActivityIndicatorView, _ viewLoading: UIView) {
        loading.startAnimating()
        loading.isHidden = false
        viewLoading.isHidden = false
        let up = ["idgroup":idgroup,"userteacher":userteacher,"newname":newname]
        socket?.emit("editGroupByTeacher", up)
    }
    
    func reditGroupByTeacher(_ loading: UIActivityIndicatorView, _ viewLoading: UIView){
        socket?.once("reditGroupByTeacher", callback: { (data, ack) in
            loading.stopAnimating()
            loading.isHidden = true
            viewLoading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            
            if (cd == 0) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditGroupSuccess"), object: nil, userInfo: ["message": "0" ])
            }
            else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "EditGroupSuccess"), object: nil, userInfo: ["message": "1" ])
            }
        })
    }
    
    // MARK: Them hoac moi sinh vien vao nhom
    func groupAddOrInviteStudentByTeacher(_ idgroup:String?, _ tuser:String?, _ suser:String?,_ loading: UIActivityIndicatorView?,_ viewLoading:UIView?) {
        loading!.startAnimating()
        loading!.isHidden = false
        viewLoading!.isHidden = false
        let up = ["idgroup":idgroup,"tuser":tuser,"suser":suser]
        socket?.emit("groupAddOrInviteStudentByTeacher", up)
    }
    
    func rgroupAddOrInviteStudentByTeacher(_ loading: UIActivityIndicatorView,_ viewLoading: UIView){
        socket?.on("rgroupAddOrInviteStudentByTeacher", callback: { (data, ack) in
            loading.isHidden = true
            viewLoading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            
            if (cd == 0) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "AddMemberSuccess"), object: nil, userInfo: ["message": "0" ])
            }
            else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "AddMemberSuccess"), object: nil, userInfo: ["message": "1" ])
            }
            
        })
    }
    
    // MARK: Xoa hoac tu choi sinh vien vao nhom
    func groupDeleteOrRefuseStudentByTeacher(_ idgroup:String, _ tuser:String, _ suser:String,_ loading: UIActivityIndicatorView,_ viewLoading:UIView){
            loading.startAnimating()
            loading.isHidden = false
            viewLoading.isHidden = false
            let up = ["idgroup":idgroup,"tuser":tuser,"suser":suser]
            socket?.emit("groupDeleteOrRefuseStudentByTeacher", up)
    }
    
    func rgroupDeleteOrRefuseStudentByTeacher(_ loading: UIActivityIndicatorView,_ viewLoading: UIView){
        socket?.once("rgroupDeleteOrRefuseStudentByTeacher", callback: { (data, ack) in
            loading.stopAnimating()
            loading.isHidden = true
            viewLoading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            if (cd == 0) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "DeleteMemberSuccess"), object: nil, userInfo: ["message": "0" ])
            }
            else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "DeleteMemberSuccess"), object: nil, userInfo: ["message": "1" ])
            }
        })
    }
    
    //  MARK: Lay thong tin tat ca de thi giang vien lam
    func getInfoAllExamTeacherMake(_ tuser:String){
        let up = ["tuser": tuser]
        socket?.emit("getInfoAllExamTeacherMake", up)
    }
    
    func rgetInfoAllExamTeacherMake(_ viewloading: UIView,_ loading: UIActivityIndicatorView){
        socket?.once("rgetInfoAllExamTeacherMake", callback: { (data, ack) in
            loading.stopAnimating()
            viewloading.isHidden = true
            loading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0]
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                let rExamTC = try JSONDecoder().decode(ResponseContest.self, from: dulieu)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoAllExamTCMake"), object: nil, userInfo: ["message":  rExamTC.data?.arr ?? "LOI"])
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    // MARK: Lay thong tin cua tat ca nhom sinh vien tham gia
    func getInfoAllGroupStudentJoin(_ suser: String?, _ loading: UIActivityIndicatorView) {
        let a = ["suser": suser]
        socket?.emit("getInfoAllGroupStudentJoin", a)
        loading.isHidden = false
        loading.startAnimating()
    }
    
    func rgetInfoAllGroupStudentJoin(_ loading: UIActivityIndicatorView) {
        socket?.once("rgetInfoAllGroupStudentJoin", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0]
            loading.stopAnimating()
            loading.isHidden = true
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                let rGroup = try JSONDecoder().decode(ResponseGroup.self, from: dulieu)
                if SocketIOManager.firstChange {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoAllGroupStudentJoin"), object: nil, userInfo: ["message": rGroup])
                    SocketIOManager.firstChange = false
                } else {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "InfoAllGroupStudentJoin_Change"), object: nil, userInfo: ["message": rGroup])
                }
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    // MARK: Thong tin tim kiem nhom
    func GroupSearchInfo(_ type:Int, _ user:String, _ loading: UIActivityIndicatorView, _ viewloading: UIView){
        if type == 0 { //type = 0 là lấy group gv quản lý
            let a = ["userteacher": user]
            socket?.emit("getInfoAllGroupTeacherManage", a)
        } else {
            let a = ["suser": user]
            socket?.emit("getInfoAllGroupStudentJoin", a)
        }
        loading.isHidden = false
        viewloading.isHidden = false
        loading.startAnimating()
    }
    
    func rGroupSearchInfo(_ type: Int,_ loading: UIActivityIndicatorView,_ viewloading: UIView){
        if type == 0 {
            socket?.once("rgetInfoAllGroupTeacherManage", callback: { (data, ack) in
                loading.stopAnimating()
                viewloading.isHidden = true
                loading.isHidden = true
                let temp1 = data as NSArray
                let res = temp1[0]
                do {
                    let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                    let rGroup = try JSONDecoder().decode(ResponseGroup.self, from: dulieu)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "GroupSearchInfoTC"), object: nil, userInfo: ["message":  rGroup .data?.arr ?? "LOI"])
                } catch {
                    print(error.localizedDescription)
                }
            })
        }
        else {
            socket?.once("rgetInfoAllGroupStudentJoin", callback: { (data, ack) in
                loading.stopAnimating()
                viewloading.isHidden = true
                loading.isHidden = true
                let temp1 = data as NSArray
                let res = temp1[0]
                do {
                    let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                    let rGroup = try JSONDecoder().decode(ResponseGroup.self, from: dulieu)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "GroupSearchInfoSV"), object: nil, userInfo: ["message":  rGroup .data?.arr ?? "LOI"])
                } catch {
                    print(error.localizedDescription)
                }
            })
        }
    }
    
    // MARK: Sinh vien thoat hoac tu choi vao nhom
    func studentExitOrRefuseGroup (_ idgroup: String?, _ suser: String?, _ viewCover: UIView?, _ loading: UIActivityIndicatorView?) {
        let up = ["idgroup": idgroup, "suser": suser]
        socket?.emit("studentExitOrRefuseGroup", up)
        loading?.isHidden = false
        loading?.startAnimating()
        viewCover?.isHidden = false
    }
    
    func rstudentExitOrRefuseGroup (_ loading: UIActivityIndicatorView, _ viewCover: UIView?) {
        socket?.once("rstudentExitOrRefuseGroup", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            loading.isHidden = true
            viewCover?.isHidden = true
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rstudentExitOrRefuseGroupSuccess"), object: nil, userInfo: ["message": "Đã thoát nhóm"])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rstudentExitOrRefuseGroupFail"), object: nil, userInfo: ["message": "Thoát nhóm thất bại"])
            }
        })
    }
    
    // MARK: Sinh vien tham gia hoac chap nhan vao nhom
    func studentJoinOrAcceptGroup (_ idgroup: String?, _ suser: String?, _ viewCover: UIView?, _ loading: UIActivityIndicatorView?) {
        let up = ["idgroup": idgroup, "suser": suser]
        socket?.emit("studentJoinOrAcceptGroup", up)
        viewCover?.isHidden = false
        loading?.isHidden = false
        loading?.startAnimating()
    }
    
    func rstudentJoinOrAcceptGroup (_ loading: UIActivityIndicatorView?, _ viewCover: UIView?) {
        socket?.on("rstudentJoinOrAcceptGroup", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            loading?.isHidden = true
            viewCover?.isHidden = true
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rstudentJoinOrAcceptGroupSuccess"), object: nil, userInfo: ["message": "Đã tham gia nhóm"])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rstudentJoinOrAcceptGroupFail"), object: nil, userInfo: ["message": "Tham gia nhóm thất bại"])
            }
        })
    }
    
    // MARK: Lay thong tin de thi (lam de)
    func getExam(_ eid:String){
        let up = ["eid": eid]
        socket?.emit("getExam", up)
    }
    
    func rgetExam(_ viewLoading: UIView, _ loading: UIActivityIndicatorView){
        socket?.once("rgetExam", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            var rExam: ResponseExam?
            loading.stopAnimating()
            loading.isHidden = true
            viewLoading.isHidden = true
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rExam = try JSONDecoder().decode(ResponseExam.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "getExamTC"), object: nil, userInfo: ["message": rExam!.data!])
            }
        })
    }
    
    // MARK: Lay thong tin cua tat ca de thi trong nhom
    func getInfoAllExamAcceptForGroup(_ gid: String?,_ loading: UIActivityIndicatorView,_ viewLoading:UIView) {
        loading.startAnimating()
       
        loading.isHidden = false
        viewLoading.isHidden = false
        let up = ["gid": gid]
        socket?.emit("getInfoAllExamAcceptForGroup", up)
    }
    
    func rgetInfoAllExamAcceptForGroup(_ loading: UIActivityIndicatorView,_ viewLoading:UIView) {
        socket?.once("rgetInfoAllExamAcceptForGroup", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            loading.isHidden = true
            viewLoading.isHidden = true
            var rExam: ResponseContest?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rExam = try JSONDecoder().decode(ResponseContest.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoAllExamAcceptForGroupSuccess"), object: nil, userInfo: ["message": rExam!.data?.arr! ?? "LOI"])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoAllExamAcceptForGroupFail"), object: nil, userInfo: ["message": "Lấy đề thất bại"])
            }
        })
    }
    
    // MARK: Lay thong tin cua sinh vien
    func getInfoOfStudent(_ suser:String){
        let up = ["suser":suser]
        socket?.emit("getInfoOfStudent", up)
    }
    
    func rgetInfoOfStudent(_ viewLoading: UIView, _ loading: UIActivityIndicatorView){
        socket?.once("rgetInfoOfStudent", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            loading.isHidden = true
            viewLoading.isHidden = true
            var rQR: ResponseLogin?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rQR = try JSONDecoder().decode(ResponseLogin.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoOfStudent"), object: nil, userInfo: ["message": rQR?.data ])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoOfStudent"), object: nil, userInfo: ["message": "Thất bại"])
            }
        })
    }
    
    // MARK: Lay thong tin giang vien
    func getInfoOfTeacher(_ tuser:String){
        let up = ["tuser":tuser]
        socket?.emit("getInfoOfTeacher", up)
    }
    
    func rgetInfoOfTeacher(_ viewLoading:UIView, _ loading:UIActivityIndicatorView){
        socket?.once("rgetInfoOfTeacher", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            loading.isHidden = true
            viewLoading.isHidden = true
            var rQR: ResponseLogin?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rQR = try JSONDecoder().decode(ResponseLogin.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoOfTeacher"), object: nil, userInfo: ["message": rQR?.data ])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetInfoOfTeacher"), object: nil, userInfo: ["message": "Thất bại"])
            }
        })
    }
    
    // MARK: Lay tat ca de thi giang vien tao
    func getDeChonLoc(_ tuser:String,_ loading: UIActivityIndicatorView,_ viewloading: UIView){
            loading.startAnimating()
            loading.isHidden = false
            viewloading.isHidden = false
            let up = ["tuser": tuser]
            socket?.emit("getInfoAllExamTeacherMake", up)
    }
    
    func rgetDeChonLoc(_ loading: UIActivityIndicatorView,_ viewloading: UIView){
        socket?.once("rgetInfoAllExamTeacherMake", callback: { (data, ack) in
            loading.stopAnimating()
            viewloading.isHidden = true
            loading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0]
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                let rExamTC = try JSONDecoder().decode(ResponseContest.self, from: dulieu)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetDeChonLoc"), object: nil, userInfo: ["message":  rExamTC.data?.arr ?? "LOI"])
            } catch {
                print(error.localizedDescription)
            }
        })
    }
    
    // MARK: Lay thoi gian thuc
    func getTimeStamp() {
        socket?.emit("getTimeStamp")
    }
    
    func rgetTimeStamp() {
        socket?.once("rgetTimeStamp", callback: { (data, ack) in
            let temp1 = data as NSArray
            let temp2 = temp1[0] as! NSDictionary
            if !StudentContestController.timeInListExam {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetTimeStamp"), object: nil, userInfo: ["message": temp2.value(forKey: "data")!])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rgetTimeStampPrepare"), object: nil, userInfo: ["message": temp2.value(forKey: "data")!])
            }
        })
    }
    
    // MARK: Lay tat ca diem cua sinh vien
    func viewAllPointOfStudent(_ suser: String, _ loading: UIActivityIndicatorView?, _ viewloading: UIView?){
        if let loading = loading, let viewloading = viewloading {
            loading.startAnimating()
            loading.isHidden = false
            viewloading.isHidden = false
        }
        let up = ["suser": suser]
        socket?.emit("viewAllPointOfStudent", up)
    }
    
    func rviewAllPointOfStudent(_ loading: UIActivityIndicatorView?, _ viewloading: UIView?){
        socket?.once("rviewAllPointOfStudent", callback: { (data, ack) in
            if let loading = loading, let viewloading = viewloading {
                loading.isHidden = true
                viewloading.isHidden = true
            }
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            var rVP: ResponseViewPointStudent?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                 rVP = try JSONDecoder().decode(ResponseViewPointStudent.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rviewAllPointOfStudent"), object: nil, userInfo: ["message": rVP?.data])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rviewAllPointOfStudentF"), object: nil, userInfo: ["message": "Thất bại"])
            }
        })
    }
    
    // MARK: Lay tat ca diem cua de thi
    func viewAllPointOfExam(_ eid:String,_ loading:UIActivityIndicatorView,_ viewloading:UIView){
        loading.startAnimating()
        loading.isHidden = false
        viewloading.isHidden = false
        let up = ["eid":eid]
        socket?.emit("viewAllPointOfExam", up)
    }
    
    func rviewAllPointOfExam(_ loading: UIActivityIndicatorView,_ viewloading:UIView){
        socket?.once("rviewAllPointOfExam", callback: { (data, ack) in
            loading.isHidden = true
            viewloading.isHidden = true
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            let cd = res.value(forKey: "cd") as! Int
            var rVP: ResponseViewPoint?
            do {
                let dulieu = try JSONSerialization.data(withJSONObject: res, options: .init())
                rVP = try JSONDecoder().decode(ResponseViewPoint.self, from: dulieu)
            } catch {
                print(error.localizedDescription)
            }
            if cd == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rviewAllPointOfExam"), object: nil, userInfo: ["message": rVP?.data])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "rviewAllPointOfExamF"), object: nil, userInfo: ["message": "Thất bại"])
            }
        })
    }
    
    // MARK: Cham diem
    func autoMask(_ alen: Int, _ aarr: [Int], _ eid: Int) {
        let up = ["alen": alen, "aarr": aarr, "eid": eid] as! [String : NSObject]
        socket?.emit("autoMask", up)
    }
    
    func rautoMask() {
        socket?.once("rautoMask", callback: { (data, ack) in
            let temp1 = data as NSArray
            let res = temp1[0] as! NSDictionary
            if (res.value(forKey: "cd") as! Int) == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "getMark"), object: nil, userInfo: ["message": res.value(forKey: "data") as! String])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "getMarkFail"), object: nil, userInfo: ["message": res.value(forKey: "msg") as! NSDictionary])
            }
        })
    }
}

