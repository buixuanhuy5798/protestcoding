//
//  RegisterController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 10/29/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift

class RegisterController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var doiTuongDangKi: UISegmentedControl!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var tfUser: UITextField!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var tfConfirmPass: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfCMT: UITextField!
    @IBOutlet weak var lblCMT: UILabel!
    @IBOutlet weak var imageCMT: UIImageView!
    @IBOutlet weak var lblErrorID: UILabel!
    @IBOutlet weak var lblErrorPass: UILabel!
    @IBOutlet weak var lblErrorPhone: UILabel!
    @IBOutlet weak var lblErrorName: UILabel!
    @IBOutlet weak var lblErrorCMT: UILabel!
    @IBOutlet weak var lblErrorXacnhan: UILabel!
    @IBOutlet weak var activityRegister: UIActivityIndicatorView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        activityRegister.isHidden = true
        btnRegister.layer.cornerRadius = 12
        btnRegister.clipsToBounds = true
        lblCMT.isHidden = true
        imageCMT.isHidden = true
        tfCMT.isHidden = true
        setUpError()
        ReceiveRegisterNoti()
    }
    
    // MARK: - Setup
    func setUpError() {
        lblErrorID.isHidden = true
        lblErrorPass.isHidden = true
        lblErrorPhone.isHidden = true
        lblErrorName.isHidden = true
        lblErrorXacnhan.isHidden = true
        lblErrorCMT.isHidden = true
    }
    
    // MARK: - Actions
    @IBAction func Action_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func YourOption(_ sender: Any) {
        if doiTuongDangKi.selectedSegmentIndex == 0 {
            lblCMT.isHidden = true
            imageCMT.isHidden = true
            tfCMT.isHidden = true
        }
        else {
            lblCMT.isHidden = false
            imageCMT.isHidden = false
            tfCMT.isHidden = false
        }
    }
    
    @IBAction func Action_Register(_ sender: Any) {
        setUpError()
        if tfUser.text == "" {
            lblErrorID.isHidden = false
            lblErrorID.text = "Tài khoản bị trống"
            self.view.makeToast("Tài khoản bị trống", duration: 3.0, position: .center)
        } else if tfPass.text == "" {
            lblErrorPass.isHidden = false
            lblErrorPass.text = "Mật khẩu bị trống"
            self.view.makeToast("Mật khẩu bị trống", duration: 3.0, position: .center)
        } else if tfConfirmPass.text == "" {
            lblErrorXacnhan.isHidden = false
            lblErrorXacnhan.text = "Xác nhận mật khẩu để trống"
            self.view.makeToast("Xác nhận mật khẩu để trống", duration: 3.0, position: .center)
        } else if tfPass.text! != tfConfirmPass.text! {
            lblErrorXacnhan.isHidden = false
            lblErrorXacnhan.text = "Xác nhận mật khẩu không khớp"
            self.view.makeToast("Xác nhận mật khẩu không khớp", duration: 3.0, position: .center)
        } else if tfPhone.text == "" {
            lblErrorPhone.isHidden = false
            lblErrorPhone.text = "Số điện thoại bị trống"
            self.view.makeToast("Số điện thoại bị trống", duration: 3.0, position: .center)
        }  else if tfName.text == "" {
            lblErrorName.isHidden = false
            lblErrorName.text = "Họ tên bị trống"
            self.view.makeToast("Họ tên bị trống", duration: 3.0, position: .center)
        } else if doiTuongDangKi.selectedSegmentIndex == 1 && tfCMT.text == "" {
            lblErrorCMT.isHidden = false
            lblErrorCMT.text = "Chứng minh thư bị trống"
            self.view.makeToast("Chứng minh thư bị trống", duration: 3.0, position: .center)
        }
        else {
            activityRegister.isHidden = false
            activityRegister.startAnimating()
            if Reachability.isConnectedToNetwork() {
                task.establishConnection {[weak self] in
                    if self?.doiTuongDangKi.selectedSegmentIndex == 0 {
                        self?.task.DangKy((self?.tfUser.text)!, (self?.tfPass.text)!, (self?.tfPhone.text)!, (self?.tfName.text)! ,"" ,0)
                    } else {
                        self?.task.DangKy((self?.tfUser.text)!, (self?.tfPass.text)!, (self?.tfPhone.text)!, (self?.tfName.text)! ,(self?.tfCMT.text)!,1)
                    }
                    self?.task.DKThanhCong((self?.activityRegister)!)
                }
            } else {
                self.view.makeToast("Không thể kết nối Internet.\n Vui lòng kiểm tra lại kết nối", duration: 3.0, position: .center)
            }
        }
    }
    
    // MARK: Supporting Methods
    func ReceiveRegisterNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(listenDK(notifi:)), name: Notification.Name(rawValue: "RegisterNoti"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DKSuccess), name: Notification.Name(rawValue: "RegisterSuccess"), object: nil)
    }
    
    @objc func listenDK(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                lblErrorID.text = msg as? String
                lblErrorID.isHidden = false
                self.view.makeToast("Tên đăng nhập đã có người sử dụng", duration: 3.0, position: .center)
            }
        }
    }
    
    @objc func DKSuccess() {
        self.view.makeToast("Đăng kí tài khoản thành công", duration: 3.0, position: .center)
    }
    
}
