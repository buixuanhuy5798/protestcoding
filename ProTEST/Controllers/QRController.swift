//
//  QRController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/23/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder
import SCLAlertView
import KeychainSwift

class QRController: QRCodeScannerViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    // MARK: - Variables
    let a = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    var ID = "0"
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func processQRCodeContent(qrCodeContent: String) -> Bool {
        let temp = String(qrCodeContent[qrCodeContent.startIndex ..< (qrCodeContent.index((qrCodeContent.startIndex), offsetBy: 2))])
        let id = String(qrCodeContent[qrCodeContent.index(qrCodeContent.startIndex, offsetBy: 2) ..< qrCodeContent.endIndex])
        ID = id
        if temp == "GV"{
            viewLoading.isHidden = false
            loading.startAnimating()
            loading.isHidden = false
            task.getInfoOfTeacher(id)
            task.rgetInfoOfTeacher(viewLoading,loading)
            ReceiveInfoTeacher()
        }
        else if temp == "SV"{
            viewLoading.isHidden = false
            loading.startAnimating()
            loading.isHidden = false
            task.getInfoOfStudent(id)
            task.rgetInfoOfStudent(viewLoading,loading)
            ReceiveInfoStudent()
        }
        else if temp == "GR"{
            let info : [String] = qrCodeContent.components(separatedBy: "\n")
            let id : String = info[1]
            let name:String = info[2]
            let member:String = info[3]
            
            if a.getBool("teacher")!{
                task.getInfoOfGroup(id,UIActivityIndicatorView(),UIView())
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificController") as? GroupSpecificController else {
                    return true
                }
                controller.GroupName = name
                controller.day = ""
                controller.IdGroup = id
                controller.member = member
                self.present(controller, animated: false, completion: nil)
            } else {
                task.getInfoOfGroup(id, UIActivityIndicatorView(),UIView())
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificControllerStudent") as? GroupSpecificControllerStudent else {
                    return true
                }
                controller.groupName = name
                controller.status = "0"
                controller.IdGroup = id
                controller.member = member
                self.present(controller, animated: true, completion: nil)
            }
        }
        return true
    }
    
    // MARK: - Supporting Methods
    func ReceiveInfoTeacher() {
        NotificationCenter.default.addObserver(self, selector: #selector(listenDK(notifi:)), name: Notification.Name(rawValue: "rgetInfoOfTeacher"), object: nil)
    }
    
    func ReceiveInfoStudent() {
        NotificationCenter.default.addObserver(self, selector: #selector(listenDK2(notifi:)), name: Notification.Name(rawValue: "rgetInfoOfStudent"), object: nil)
    }
    
    @objc func listenDK(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? DataLogin
                let sb = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = sb.instantiateViewController(withIdentifier: "SearchInfoTeacher") as? SearchInfoTeacher else {
                    return
                }
                controller.gv = true
                controller.ID = ID
                controller.name = temp?.name
                controller.phone = temp?.phone
                self.present(controller, animated: true, completion: nil)
            }
        }
    }

    @objc func listenDK2(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? DataLogin
                let sb = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = sb.instantiateViewController(withIdentifier: "SearchInfoTeacher") as? SearchInfoTeacher else {
                    return
                }
                controller.gv = false
                controller.ID = ID
                controller.name = temp?.name
                controller.phone = temp?.phone
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
}
