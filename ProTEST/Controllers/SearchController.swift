//
//  SearchController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/2/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift
import KeychainSwift

class SearchController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tbViewResult: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var viewSearching: UIView!
    @IBOutlet weak var viewCovering: UIView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var resultReturnFromServer: DataSearch?
    var searchTimer: Timer?
    var btnCancel: Bool?
    var typeSearch = [String]()
    var rowForSection = [[Arr]]()
    var t = [Arr]()
    var g = [Arr]()
    var s = [Arr]()
    var indexSegment: Int?
    let a = KeychainSwift.init()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldSearch.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        tbViewResult.isHidden = true
        btnCancel = false
        viewSearching.isHidden = true
        viewCovering.isHidden = true
        viewSearching.alpha = 0.5
        viewCovering.alpha = 0.5
        activityIndicator.isHidden = true
        setUpTableViewResult()
    }
    
    // MARK: - Setups
    func setUpTableViewResult() {
        tbViewResult.register(UINib(nibName: "ResultSearchingCell_Teacher", bundle: nil), forCellReuseIdentifier: "ResultSearchingCell_Teacher")
        tbViewResult.register(UINib(nibName: "ResultSearchCell_Group", bundle: nil), forCellReuseIdentifier: "ResultSearchCell_Group")
        tbViewResult.register(UINib(nibName: "ResultSearchCell_Student", bundle: nil), forCellReuseIdentifier: "ResultSearchCell_Student")
        tbViewResult.dataSource = self
        tbViewResult.delegate = self
        textFieldSearch.delegate = self
    }
    
    // MARK: - Action
    @IBAction func changeSegment(_ sender: Any) {
        if !(textFieldSearch.text?.isEmpty)! {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            viewSearching.isHidden = false
            viewCovering.isHidden = false
            t.removeAll(); g.removeAll(); s.removeAll()
            rowForSection.removeAll()
            typeSearch.removeAll()
            tbViewResult.isHidden = true
            if segmentControl.selectedSegmentIndex == 0 {
                task.pushSearchText("all", textFieldSearch.text!)
            } else if segmentControl.selectedSegmentIndex == 1 {
                task.pushSearchText("teacher", textFieldSearch.text!)
            } else if segmentControl.selectedSegmentIndex == 2 {
                task.pushSearchText("student", textFieldSearch.text!)
            } else {
                task.pushSearchText("group", textFieldSearch.text!)
            }
            indexSegment = segmentControl.selectedSegmentIndex
            textFieldSearch.resignFirstResponder()
            task.getSearchData(self.activityIndicator, viewSearching, viewCovering)
            ReceiveSearchResult()
        }
    }
    
    // MARK: - Actions
    @IBAction func btnQRSearch(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = sb.instantiateViewController(withIdentifier: "QR") as? QR else {
            return
        }
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func ActionCancel(_ sender: Any) {
        btnCancel = true
        tbViewResult.reloadData()
        tbViewResult.isHidden = true
        viewSearching.isHidden = true
        viewCovering.isHidden = true
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        textFieldSearch.text = ""
        textFieldSearch.placeholder = "Nhập thông tin tìm kiếm"
        textFieldSearch.endEditing(true)
    }
    
    @IBAction func showtblView2(_ sender: Any) {
        tbViewResult.isHidden = false
    }
    
    // MARK: - Supporting Methods
    func ReceiveSearchResult() {
        NotificationCenter.default.addObserver(self, selector: #selector(returnKQ(notifi:)), name: Notification.Name(rawValue: "SearchResult"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(listenSearchNoti(notifi:)), name: Notification.Name(rawValue: "SearchNoti"), object: nil)
    }

    @objc func returnKQ(notifi :Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let tmp = msg as! ResponseSearch
                resultReturnFromServer = tmp.data
                for teacher in (resultReturnFromServer?.arr)! {
                    if indexSegment == 0 {
                        if teacher.type! == "teacher" {
                            if !typeSearch.contains("Giảng viên") {
                                typeSearch.append("Giảng viên")
                            }
                            t.append(teacher)
                        } else if teacher.type! == "group"{
                            if !typeSearch.contains("Nhóm") {
                                typeSearch.append("Nhóm")
                            }
                            g.append(teacher)
                        } else {
                            if !typeSearch.contains("Sinh viên") {
                                typeSearch.append("Sinh viên")
                            }
                            s.append(teacher)
                        }
                    } else if indexSegment == 1 {
                        if teacher.type! == "teacher" {
                            if !typeSearch.contains("Giảng viên") {
                                typeSearch.append("Giảng viên")
                            }
                            t.append(teacher)
                        }
                    } else if indexSegment == 2 {
                        if teacher.type! == "student"{
                            if !typeSearch.contains("Sinh viên") {
                                typeSearch.append("Sinh viên")
                            }
                            s.append(teacher)
                        }
                    } else {
                        if teacher.type! == "group"{
                            if !typeSearch.contains("Nhóm") {
                                typeSearch.append("Nhóm")
                            }
                            g.append(teacher)
                        }
                    }
                }
                if indexSegment == 0 {
                    if typeSearch.isEmpty {
                        self.view.makeToast("Không tìm thấy kết quả phù hợp", duration: 3.0, position: .center)
                    } else {
                        for type in typeSearch {
                            if type == "Giảng viên" {
                                rowForSection.append(t)
                            } else if type == "Sinh viên" {
                                rowForSection.append(s)
                            } else {
                                rowForSection.append(g)
                            }
                        }
                    }
                } else if indexSegment == 1 {
                    if typeSearch.isEmpty {
                        self.view.makeToast("Không tìm thấy kết quả phù hợp", duration: 3.0, position: .center)
                    } else {
                        rowForSection.append(t)
                    }
                } else if indexSegment == 2 {
                    if typeSearch.isEmpty {
                        self.view.makeToast("Không tìm thấy kết quả phù hợp", duration: 3.0, position: .center)
                    } else {
                        rowForSection.append(s)
                    }
                } else {
                    if typeSearch.isEmpty {
                        self.view.makeToast("Không tìm thấy kết quả phù hợp", duration: 3.0, position: .center)
                    } else {
                        rowForSection.append(g)
                    }
                }
                btnCancel = false
                tbViewResult.isHidden = false
                tbViewResult.reloadData()
            }
        }
    }
    
    @objc func listenSearchNoti(notifi :Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                self.view.makeToast("\(msg)", duration: 3.0, position: .center)
            }
        }
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        
        // retrieve the keyword from user info
        let keyword = timer.userInfo!
        textFieldSearch.resignFirstResponder()
        var type = "all"
        indexSegment = 0
        tbViewResult.isHidden = true
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        viewSearching.isHidden = false
        viewCovering.isHidden = false
        if segmentControl.selectedSegmentIndex == 0 {
            type = "all"
            indexSegment = 0
        } else if segmentControl.selectedSegmentIndex == 1 {
            type = "teacher"
            indexSegment = 1
        } else if segmentControl.selectedSegmentIndex == 2 {
            type = "student"
            indexSegment = 2
        } else {
            type = "group"
            indexSegment = 3
        }
        t.removeAll(); g.removeAll(); s.removeAll()
        rowForSection.removeAll()
        typeSearch.removeAll()
        task.pushSearchText(type, keyword as! String)
        task.getSearchData(self.activityIndicator, viewSearching, viewCovering)
        ReceiveSearchResult()
    }
}

// MARK: Extension
extension SearchController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowForSection[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return typeSearch.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return typeSearch[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexSegment == 0 {
            if indexPath.section == 0 {
                if typeSearch[indexPath.section] == "Giảng viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchingCell_Teacher") as? ResultSearchingCell_Teacher else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Teacher((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!, (rowForSection[indexPath.section][indexPath.row].cmnd)!)
                    } else {
                        cell.setDuLieu_Teacher("", "", "", "")
                    }
                    return cell
                } else if typeSearch[indexPath.section] == "Sinh viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Student") as? ResultSearchCell_Student else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Student((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!)
                    } else {
                        cell.setDuLieu_Student("", "", "")
                    }
                    return cell
                } else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Group") as? ResultSearchCell_Group else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Group((rowForSection[indexPath.section][indexPath.row].tname)!, (rowForSection[indexPath.section][indexPath.row].gdate)!, (rowForSection[indexPath.section][indexPath.row].tuser)!, String(rowForSection[indexPath.section][indexPath.row].item!), (rowForSection[indexPath.section][indexPath.row].gname)!)
                    } else {
                        cell.setDuLieu_Group("", "", "", "", "")
                    }
                    return cell
                }
            } else if indexPath.section == 1 {
                if typeSearch[indexPath.section] == "Giảng viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchingCell_Teacher") as? ResultSearchingCell_Teacher else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Teacher((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!, (rowForSection[indexPath.section][indexPath.row].cmnd)!)
                    } else {
                        cell.setDuLieu_Teacher("", "", "", "")
                    }
                    return cell
                } else if typeSearch[indexPath.section] == "Sinh viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Student") as? ResultSearchCell_Student else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Student((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!)
                    } else {
                        cell.setDuLieu_Student("", "", "")
                    }
                    return cell
                } else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Group") as? ResultSearchCell_Group else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Group((rowForSection[indexPath.section][indexPath.row].tname)!, (rowForSection[indexPath.section][indexPath.row].gdate)!, (rowForSection[indexPath.section][indexPath.row].tuser)!, String(rowForSection[indexPath.section][indexPath.row].item!), (rowForSection[indexPath.section][indexPath.row].gname)!)
                    } else {
                        cell.setDuLieu_Group("", "", "", "", "")
                    }
                    return cell
                }
            } else {
                if typeSearch[indexPath.section] == "Giảng viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchingCell_Teacher") as? ResultSearchingCell_Teacher else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Teacher((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!, (rowForSection[indexPath.section][indexPath.row].cmnd)!)
                    } else {
                        cell.setDuLieu_Teacher("", "", "", "")
                    }
                    return cell
                } else if typeSearch[indexPath.section] == "Sinh viên" {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Student") as? ResultSearchCell_Student else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Student((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!)
                    } else {
                        cell.setDuLieu_Student("", "", "")
                    }
                    return cell
                } else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Group") as? ResultSearchCell_Group else {
                        return UITableViewCell()
                    }
                    if !btnCancel! {
                        cell.setDuLieu_Group((rowForSection[indexPath.section][indexPath.row].tname)!, (rowForSection[indexPath.section][indexPath.row].gdate)!, (rowForSection[indexPath.section][indexPath.row].tuser)!, String(rowForSection[indexPath.section][indexPath.row].item!), (rowForSection[indexPath.section][indexPath.row].gname)!)
                    } else {
                        cell.setDuLieu_Group("", "", "", "", "")
                    }
                    return cell
                }
            }
        } else if indexSegment == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchingCell_Teacher") as? ResultSearchingCell_Teacher else {
                return UITableViewCell()
            }
            if !btnCancel! && typeSearch.contains("Giảng viên"){
                cell.setDuLieu_Teacher((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!, (rowForSection[indexPath.section][indexPath.row].cmnd)!)
            } else {
                cell.setDuLieu_Teacher("", "", "", "")
            }
            return cell
        } else if indexSegment == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Student") as? ResultSearchCell_Student else {
                return UITableViewCell()
            }
            if !btnCancel! && typeSearch.contains("Sinh viên") {
                cell.setDuLieu_Student((rowForSection[indexPath.section][indexPath.row].name)!, (rowForSection[indexPath.section][indexPath.row].phone)!, (rowForSection[indexPath.section][indexPath.row].itemhuman)!)
            } else {
                cell.setDuLieu_Student("", "", "")
            }
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultSearchCell_Group") as? ResultSearchCell_Group else {
                return UITableViewCell()
            }
            if !btnCancel! && typeSearch.contains("Nhóm") {
                cell.setDuLieu_Group((rowForSection[indexPath.section][indexPath.row].tname)!, (rowForSection[indexPath.section][indexPath.row].gdate)!, (rowForSection[indexPath.section][indexPath.row].tuser)!, String(rowForSection[indexPath.section][indexPath.row].item!), (rowForSection[indexPath.section][indexPath.row].gname)!)
            } else {
                cell.setDuLieu_Group("", "", "", "", "")
            }
            return cell
        }
    }
}

extension SearchController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexSegment == 3 || (indexSegment == 0 && typeSearch[indexPath.section] == "Nhóm") {
            if a.getBool("teacher")!{
                
                    task.getInfoOfGroup(String(rowForSection[indexPath.section][indexPath.row].item!),UIActivityIndicatorView(),UIView())
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificController") as? GroupSpecificController else {
                            return
                    }
                
                    controller.GroupName = rowForSection[indexPath.section][indexPath.row].gname
                    controller.day = rowForSection[indexPath.section][indexPath.row].gdate
                    controller.IdGroup = String(rowForSection[indexPath.section][indexPath.row].item!)
                    controller.member = "12"
                
                    self.present(controller, animated: false, completion: nil)
            }
            else {
                task.getInfoOfGroup(String(rowForSection[indexPath.section][indexPath.row].item!), UIActivityIndicatorView(),UIView())
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificControllerStudent") as? GroupSpecificControllerStudent else {
                    return
                }
                controller.groupName = rowForSection[indexPath.section][indexPath.row].gname
                controller.status = "0"
                controller.IdGroup = String(rowForSection[indexPath.section][indexPath.row].item!)
                controller.member = "12"
                self.present(controller, animated: true, completion: nil)
            }
        
        } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "SearchInfoTeacher") as? SearchInfoTeacher else {
                    return
                }
                controller.ID = rowForSection[indexPath.section][indexPath.row].itemhuman
                controller.name = rowForSection[indexPath.section][indexPath.row].name
                controller.phone = rowForSection[indexPath.section][indexPath.row].phone
                if (typeSearch[indexPath.section] == "Giảng viên"){
                    controller.gv = true
                }
                else {
                    controller.gv = false
                }
                self.present(controller, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension SearchController: UITextFieldDelegate {
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        
        // if a timer is already active, prevent it from firing
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
   
        if !(textField.text?.isEmpty)! {
            searchTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text!, repeats: false)
        }
        btnCancel = true
        tbViewResult.reloadData()
        tbViewResult.isHidden = true
    }
}
