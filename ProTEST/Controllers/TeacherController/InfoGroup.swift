//
//  InfoGroup.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import SCLAlertView
import QRCoder

class InfoGroup: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var lblDGroup: UILabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblTeacherUser: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var loadding: UIActivityIndicatorView!
    @IBOutlet weak var viewLoad: UIView!
    
    // MARK: - Variables
    static var cuatoi = true
    var IDGroup:String!
    var nameGroup:String!
    var TeacherName:String!
    var TeacherUser:String!
    var Day:String!
    var Member:String!
    var name:String?
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        ReceivereditGroup()
        ReceiveDeleteGroup()
    }
    
    // MARK: - Actions
    @IBAction func btnEdit(_ sender: Any) {
        let alert = SCLAlertView()
        let txt = alert.addTextField("Tên mới:  ")
        alert.addButton("OK") {
            self.task.editGroupByTeacher(self.IDGroup, self.TeacherUser, txt.text!, self.loadding, self.viewLoad)
            self.name = txt.text
            self.task.reditGroupByTeacher(self.loadding, self.viewLoad)
            self.view.makeToast("Yêu cầu đang được xử lý", duration: 1.0, position: .center)
        }
        alert.showTitle( "Đổi tên nhóm", subTitle: String() , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        let alert = SCLAlertView()
        alert.addButton("OK") {
            self.task.deleteGroupByTeacher(self.IDGroup, self.TeacherUser,self.loadding, self.viewLoad)
            self.task.rdeleteGroupByTeacher(self.loadding, self.viewLoad)
            self.view.makeToast("Yêu cầu đang được xử lý", duration: 1.0, position: .center)
        }
        alert.showTitle( "Xoá nhóm ", subTitle: "Có chắc chắn muốn xoá nhóm ?" , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        
    }
    
    // MARK: - Setup
    func setUp(){
        if InfoGroup.cuatoi {
            viewCover.isHidden = false
        }
        else {
            viewCover.isHidden = true
        }
        let generator = QRCodeGenerator()
        imageQR.image = generator.createImage(value: "GR\n" + IDGroup + "\n" + nameGroup + "\n" + Member + "\n" + Day ,size: imageQR.frame.size)
        if (Day == ""){
            lblDay.text = "Không xem được thông tin này"
        } else {
            lblDay.text = getDateFromTimeStamp(Day)
        }
        lblDGroup.text = IDGroup
        lblMember.text = Member
        lblTeacherName.text = TeacherName
        lblTeacherUser.text = TeacherUser
    }
    
    // MARK: - Supporting Methods
    func ReceiveDeleteGroup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDelete(notifi:)), name: Notification.Name(rawValue:"DeleteGroupSuccess"),object: nil)
    }
    
    func getDateFromTimeStamp(_ unix:String) -> String {
        let timeStamp = Double(unix)
        let date = NSDate(timeIntervalSince1970: timeStamp!)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    @objc func updateDelete(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    self.view.makeToast("Xoa nhom thanh cong", duration: 2.5, position: .bottom)
                
                    dismiss(animated: false, completion: nil)
                } else {
                     self.view.makeToast("Xoa nhom that bai", duration: 2.5, position: .bottom)
                }
            }
        }
    }
    
    func ReceivereditGroup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateEdit(notifi:)), name: Notification.Name(rawValue:"EditGroupSuccess"),object: nil)
    }
    
    @objc func updateEdit(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "UpdateName"), object: nil, userInfo: ["message": name ?? "" ])
                    self.view.makeToast("Sửa thông tin thành công", duration: 2.5, position: .bottom)
                } else {
                    self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
                }
            }
        }
    }
}
