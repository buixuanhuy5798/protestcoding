//
//  ContestController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ContestController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tbvContest: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!
    
    // MARK: - Variables
    var DataExam = [ArrContest]()
    var idGroup:String!
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        ReceiveAllContest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        task.getInfoAllExamAcceptForGroup(idGroup,loading,viewLoading)
        task.rgetInfoAllExamAcceptForGroup(loading, viewLoading)
    }
    
    // MARK: - Supporting Methods
    func ReceiveAllContest() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateContest(notifi:)), name: Notification.Name(rawValue:"rgetInfoAllExamAcceptForGroupSuccess"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateContestfail(notifi:)), name: Notification.Name(rawValue:"rgetInfoAllExamAcceptForGroupFail"),object: nil)
    }
    
    @objc func updateContest(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? [ArrContest]{
                    DataExam = temp
                } else{
                    print("LOI")
                }
                
            }
        }
        tbvContest.reloadData()
    }
    
    @objc func updateContestfail(notifi : Notification) {
        if let mess = notifi.userInfo {
            if mess["message"] != nil {
                print("LOI2")
                
            }
        }
    }
    
    @objc func buttonChitiet(sender: UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ExamTeacher") as? ExamTeacher  else {
            return
        }
        controller.id = DataExam[sender.tag].eid
        controller.NameExam = DataExam[sender.tag].name
        task.getExam(DataExam[sender.tag].eid!)
        self.present(controller, animated: false, completion: nil)
    }
    
    // MARK: Setup
    func setData(){
        tbvContest.dataSource = self
        tbvContest.delegate = self
        tbvContest.register(UINib(nibName:"ExamTeacherMake",bundle:nil), forCellReuseIdentifier: "ExamTeacherMake")
    }
}

// MARK: - Extensions
extension ContestController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataExam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExamTeacherMake") as? ExamTeacherMake else {
            return UITableViewCell()
        }
        cell.btnChitiet.tag = indexPath.row
        cell.btnChitiet.addTarget(self, action: #selector(buttonChitiet), for: .touchUpInside)
        cell.setCell(DataExam[indexPath.row].name, DataExam[indexPath.row].created, DataExam[indexPath.row].type, DataExam[indexPath.row].qlen, DataExam[indexPath.row].publish, DataExam[indexPath.row].eid, DataExam[indexPath.row].tuser, DataExam[indexPath.row].timeStart, DataExam[indexPath.row].timeEnd)

        return cell
    }
}

extension ContestController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
