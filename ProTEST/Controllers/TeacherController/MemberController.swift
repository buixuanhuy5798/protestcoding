//
//  MemberController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import MenuButton

class MemberController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var Menu: MenuButtonView!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblTeacherID: UILabel!
    @IBOutlet weak var tbvMember: UITableView!
    @IBOutlet weak var lblMemberRQ: UILabel!
    @IBOutlet weak var viewTeacher: UIView!
    
    // MARK: - Variables
    var MemberGroup = [Member]()
    var Memberinvited = [Member]()
    var MemberRequest = [Member]()
    var TeacherName:String!
    var TeacherID:String!
    var idGroup:String!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTeacherName.text = TeacherName
        lblTeacherID.text = TeacherID
        lblMemberRQ.text  = "\(MemberRequest.count)"
        setUp()
        configureMenuButtonView()
    }

    // MARK: - Actions
    @IBAction func btnRequestAccept(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MemberRequest") as! MemberRequestController_TC
        viewController.MemberRequest = MemberRequest
        viewController.idgroup = idGroup
        viewController.tuser = TeacherID
        present(viewController, animated: false, completion: nil)
    }
    
    // MARK: - Setup
    func setUp(){
        tbvMember.register(UINib(nibName: "MemberCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        tbvMember.dataSource = self
        tbvMember.delegate = self
    }
    
    func configureMenuButtonView() {
        Menu.bindView(self.view)
        Menu.onItems = makeItems()
        Menu.onConfigure = makeConfiguration()
    }
    
    func makeItems() -> (() -> [MenuItem])  {
        return {
            return [
                MenuItem(image: UIImage(named: "delete") ?? UIImage(), text: "Xoá thành viên", action: { self.deleteMember() }),
                MenuItem(image: UIImage(named: "add") ?? UIImage(), text: "Thêm thành viên mới", action: { self.addMember() }),
                
            ]
        }
    }
    
    private func makeConfiguration() -> (() -> [MenuButtonViewConfig]) {
        return {
            return [
                .strokeColor(#colorLiteral(red: 0.2901960784, green: 0.7803921569, blue: 0.5843137255, alpha: 1)),
                .borderStrokeColor(#colorLiteral(red: 0.2901960784, green: 0.7803921569, blue: 0.5843137255, alpha: 1)),
                .textMenuColor(#colorLiteral(red: 0.2901960784, green: 0.7803921569, blue: 0.5843137255, alpha: 1)),
                .menuCellHeight(50),
                .menuWidth(200.0),
                ]
        }
    }
    
    // MARK: - Supporting Methods
    func addMember(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AddMemberController") as! AddMemberController
        viewController.IDgroup = idGroup
        present(viewController, animated: false, completion: nil)
    }
    
    func deleteMember(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "XoaMemberTeacher") as! XoaMemberTeacher
        viewController.MemberGroup = MemberGroup
        viewController.idGroup = idGroup
        viewController.tuser = TeacherID
        present(viewController, animated: false, completion: nil)
    }
}

// MARK: - Extensions
extension MemberController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MemberGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as? MemberCell else {
            return UITableViewCell()
        }
        cell.setMember(MemberGroup[indexPath.row].name, MemberGroup[indexPath.row].user)
        return cell
    }
}

extension MemberController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewTeacher.frame.height
    }

}
