
import UIKit

class AllExamTeacherController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var tbvExam: UITableView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var DataExam = [ArrContest]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loading.startAnimating()
        task.rgetInfoAllExamTeacherMake(viewLoading, loading)
        setData()
        ReceiveAllExamTeacherMake()
    }
    
    // MARK: - Supporting Methods
    func ReceiveAllExamTeacherMake() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateExamTCMake(notifi:)), name: Notification.Name(rawValue:"InfoAllExamTCMake"),object: nil)
    }
    
    @objc func updateExamTCMake(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? [ArrContest]{
                    DataExam = temp
                } else{
                    print("LOI")
                }
            }
        }
        tbvExam.reloadData()
    }
    
    @objc func buttonChitiet(sender: UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ExamTeacher") as? ExamTeacher  else {
            return
        }
        
        controller.id = DataExam[sender.tag].eid
        controller.NameExam = DataExam[sender.tag].name
        task.getExam(DataExam[sender.tag].eid!)
        self.present(controller, animated: false, completion: nil)
    }

    // MARK: - Setup
    func setData(){
        tbvExam.dataSource = self
        tbvExam.delegate = self
        tbvExam.register(UINib(nibName:"ExamTeacherMake",bundle:nil), forCellReuseIdentifier: "ExamTeacherMake")
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
}

// MARK: - Extensions
extension AllExamTeacherController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataExam.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExamTeacherMake") as? ExamTeacherMake else {
            return UITableViewCell()
        }
        cell.btnChitiet.tag = indexPath.row
        cell.btnChitiet.addTarget(self, action: #selector(buttonChitiet), for: .touchUpInside)
        cell.setCell(DataExam[indexPath.row].name, DataExam[indexPath.row].created, DataExam[indexPath.row].type, DataExam[indexPath.row].qlen, DataExam[indexPath.row].publish, DataExam[indexPath.row].eid, DataExam[indexPath.row].tuser, DataExam[indexPath.row].timeStart, DataExam[indexPath.row].timeEnd)
        return cell
    }

}

extension AllExamTeacherController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
