//
//  MemberRequestController_TC.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/13/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class MemberRequestController_TC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var tbvRequest: UITableView!
    
    // MARK: - Variables
    var MemberRequest = [Member]()
    let task = SocketIOManager.sharedInstance
    var tuser: String!
    var idgroup: String!
    var index: Int!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        ReceiverAddMember()
        ReceiverDeleteMember()
    }
    
    // MARK: - Actions
    @IBAction func Back(_ sender: Any) {
        PageGroupController.change = true
        dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Setup
    func setUp(){
        tbvRequest.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "MemberCellRequest")
        tbvRequest.dataSource = self
        tbvRequest.delegate = self
    }
    
    // MARK: - Supporting Methods
    func ReceiverDeleteMember(){
        NotificationCenter.default.addObserver(self, selector: #selector(updatedeleteMember(notifi:)), name: Notification.Name(rawValue:"DeleteMemberSuccess"),object: nil)
    }
    
    func ReceiverAddMember() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateAddMember(notifi:)), name: Notification.Name(rawValue:"AddMemberSuccess"),object: nil)
    }
    
    @objc func buttonAccept(sender: UIButton!) {
        index = sender.tag
        task.groupAddOrInviteStudentByTeacher(idgroup, tuser, MemberRequest[sender.tag].user, loading, viewLoading)
        task.rgroupAddOrInviteStudentByTeacher(loading, viewLoading)
    }
    
    @objc func buttonDeny(sender: UIButton){
        index = sender.tag
        task.groupDeleteOrRefuseStudentByTeacher(idgroup, tuser, MemberRequest[sender.tag].user, loading, viewLoading)
        task.rgroupDeleteOrRefuseStudentByTeacher(loading, viewLoading)
    }
    
    @objc func updatedeleteMember(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    self.view.makeToast("Đã từ chối thành viên", duration: 2.5, position: .bottom)
                    MemberRequest.remove(at: index)
                    tbvRequest.reloadData()
                    PageGroupController.change = true
                } else {
                    self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
                }
            }
        }
    }
    
    @objc func updateAddMember(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    self.view.makeToast("Đã thêm thành viên mới", duration: 2.5, position: .bottom)
                    self.dismiss(animated: true, completion: nil)
                    tbvRequest.reloadData()
                } else {
                    self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
                }
            }
        }
    }
}

// MARK: - Extensions
extension MemberRequestController_TC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MemberRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         guard  let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCellRequest") as? RequestCell else {
            return UITableViewCell()
        }
        cell.setMemberRequest(MemberRequest[indexPath.row].name, MemberRequest[indexPath.row].user)
        cell.btnAccept.tag = indexPath.row
        cell.btnDeny.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(buttonAccept), for: .touchUpInside)
        cell.btnDeny.addTarget(self, action:#selector(buttonDeny), for: .touchUpInside)
        return cell
    }
    
}

extension MemberRequestController_TC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
}
