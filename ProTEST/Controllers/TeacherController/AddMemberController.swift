//
//  AddMemberController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import KeychainSwift

class AddMemberController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var tbvAdd: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var viewCovering: UIView!
    @IBOutlet weak var viewCoverAdding: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    // MARK: - Variables
    let keychain = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    var resultReturnFromServer: DataSearch?
    var searchTimer: Timer?
    var btnCancel: Bool?
    var IDgroup: String?
    var student = [Arr]()
    var index:Int!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tfSearch.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        tbvAdd.isHidden = true
        btnCancel = false
        viewCovering.isHidden = true
        viewCoverAdding.isHidden = true
        viewCoverAdding.alpha = 0.5
        viewCovering.alpha = 0.5
        loading.isHidden = true
        setUpTableViewResult()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.task.rgroupAddOrInviteStudentByTeacher(loading, viewCoverAdding)
        AddSuccessOrFail()
    }
    
    // MARK: - Actions
    @IBAction func btnCancel(_ sender: Any) {
        btnCancel = true
        tbvAdd.reloadData()
        tbvAdd.isHidden = true
        viewCovering.isHidden = true
        loading.stopAnimating()
        loading.isHidden = true
        tfSearch.text = nil
        tfSearch.placeholder = "Nhập thông tin tìm kiếm"
        tfSearch.endEditing(true)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func showTableView2(_ sender: Any) {
        tbvAdd.isHidden = false
    }
    
    // MARK: - Setup
    func setUpTableViewResult() {
        tbvAdd.register(UINib(nibName: "AddMemberGroupTC", bundle: nil), forCellReuseIdentifier: "AddMemberGroupTC")
        tbvAdd.dataSource = self
        tbvAdd.delegate = self
        tfSearch.delegate = self
    }
    
    // MARK: - Supporting Methods
    func ReceiveSearchResult() {
        NotificationCenter.default.addObserver(self, selector: #selector(returnKQ(notifi:)), name: Notification.Name(rawValue: "SearchResult"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(listenSearchNoti(notifi:)), name: Notification.Name(rawValue: "SearchNoti"), object: nil)
    }
    
    func AddSuccessOrFail() {
        NotificationCenter.default.addObserver(self, selector: #selector(AddOrReject(notifi:)), name: Notification.Name(rawValue: "AddMemberSuccess"), object: nil)
    }
    
    @objc func AddOrReject(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let res = msg as! String
                if res == "0" {
                    viewLoading.isHidden = true
                    self.view.makeToast("Đã thêm sinh viên thành công", duration: 1.0, position: .center)
                    
                    
                } else {
                    self.view.makeToast("Sinh viên đã có trong nhóm", duration: 1.0, position: .center)
                }
            }
        }
    }
    
    @objc func returnKQ(notifi :Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let tmp = msg as! ResponseSearch
                resultReturnFromServer = tmp.data
                student = (resultReturnFromServer?.arr)!
                if student.isEmpty {
                    self.view.makeToast("Không tìm thấy kết quả phù hợp", duration: 3.0, position: .center)
                }
                btnCancel = false
                tbvAdd.isHidden = false
                tbvAdd.reloadData()
            }
        }
    }
    
    @objc func listenSearchNoti(notifi :Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                self.view.makeToast("\(msg)", duration: 3.0, position: .center)
            }
        }
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        // retrieve the keyword from user info
        let keyword = timer.userInfo!
        tfSearch.resignFirstResponder()
        tbvAdd.isHidden = true
        loading.isHidden = false
        loading.startAnimating()
        viewCovering.isHidden = false
        student.removeAll()
        task.pushSearchText("student", keyword as! String)
        task.getSearchData(self.loading, nil, viewCovering)
        ReceiveSearchResult()
    }
}

// MARK: - Extensions
extension AddMemberController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return student.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberGroupTC") as? AddMemberGroupTC else {
            return UITableViewCell()
        }
        
        if !btnCancel! {
            cell.setDataFromServer(UIImage(named: "QR code example.jpg"), student[indexPath.row].name, student[indexPath.row].itemhuman)
            cell.loading = loading
            cell.tUser = keychain.get("username")
            cell.sUser = student[indexPath.row].itemhuman
            cell.viewLoading = viewCoverAdding
            cell.idGroup = IDgroup
        } else {
            cell.setDataFromServer(nil, "", "")
        }
        return cell
    }
}

extension AddMemberController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension AddMemberController: UITextFieldDelegate {
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        
        // if a timer is already active, prevent it from firing
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        
        // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
        textField.text = textField.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " "))
        if !(textField.text?.isEmpty)! {
            searchTimer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text!, repeats: false)
        }
        btnCancel = true
        tbvAdd.reloadData()
        tbvAdd.isHidden = true
    }
}
