//
//  GroupController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/3/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import SCLAlertView
import KeychainSwift

class GroupController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tbvGroup: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!

    // MARK: - Variables
    var id: String!
    var dataGroup: DataGroup!
    static var change = false
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        ReceiveDeleteGroup()
        ReceiveCreatGroup()
        ReceiveGroupNoti()
        ReceiveUpdateName()
        ReceiveGroupNotiChange()
    }

    // MARK: - Setups
    func setUp() {
        tbvGroup.register(UINib(nibName: "GroupCellTeacher", bundle: nil), forCellReuseIdentifier: "GroupCellTeacher")
        tbvGroup.dataSource = self
        tbvGroup.delegate = self
    }
    
    // MARK: - Supporting Methods
    func ReloadData() {
        viewLoading.isHidden = false
        loading.startAnimating()
        loading.isHidden = false
        task.getInfoAllGroupTeacherManage(id,UIActivityIndicatorView())
        task.rgetInfoAllGroupTeacherMange(UIActivityIndicatorView())
    }
    
    func ReceiveCreatGroup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateCreatGroup(notifi:)), name: Notification.Name(rawValue:"CreatGroupSuccess"),object: nil)
    }
    
    func ReceiveGroupNoti() {
        viewLoading.isHidden = true
        loading.stopAnimating()
        loading.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroup(notifi:)), name: Notification.Name(rawValue:"InfoGroupTeacherManage"),object: nil)
    }
    
    func ReceiveGroupNotiChange() {
        viewLoading.isHidden = true
        loading.stopAnimating()
        loading.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroup(notifi:)), name: Notification.Name(rawValue:"InfoGroupTeacherManageChange"),object: nil)
    }
    
    func ReceiveDeleteGroup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDelete(notifi:)), name: Notification.Name(rawValue:"DeleteGroupSuccess"),object: nil)
    }
    
    func ReceiveUpdateName(){
        NotificationCenter.default.addObserver(self, selector: #selector(updateName(notifi:)), name: Notification.Name(rawValue:"UpdateName"),object: nil)
    }
    
    @objc func updateName(notifi : Notification) {
        ReloadData()
    }
    
    @objc func updateCreatGroup(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    self.view.makeToast("Tạo nhóm thành công", duration: 2.5, position: .bottom)
                    ReloadData()
                } else {
                    self.view.makeToast("Tạo nhóm thất bại ", duration: 2.5, position: .bottom)
                }
            }
        }
    }
    
    @objc func updateGroup(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? DataGroup
          
                dataGroup = temp
                viewLoading.isHidden = true
                loading.isHidden = true
                tbvGroup.reloadData()
            }
        }
    }
    
    @objc func updateDelete(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! String
                if (temp == "0") {
                    ReloadData()
                }
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func btnCreatGroup(_ sender: Any) {
        let alert = SCLAlertView()
        let txt = alert.addTextField("Nhập tên nhóm : ")
         alert.addButton("OK") {
            self.task.createGroup(self.id, txt.text!, self.loading, self.viewLoading)
            self.task.rcreatGroup(self.loading, self.viewLoading)
            self.view.makeToast("Yêu cầu đang được xử lý", duration: 1.0, position: .center)
        }
         alert.showTitle( "Tạo nhóm mới", subTitle: String() , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
    }
}

// MARK: - Extension
extension GroupController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataGroup.arr!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCellTeacher") as? GroupCellTeacher else {
            return UITableViewCell()
        }
        cell.set(dataGroup.arr![indexPath.row].gname, dataGroup.arr![indexPath.row].gid, dataGroup.arr![indexPath.row].gmember, dataGroup.arr![indexPath.row].gdate)
        return cell
    }
}

extension GroupController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height / 7.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        task.getInfoOfGroup(String(dataGroup.arr![indexPath.row].gid!),UIActivityIndicatorView(),UIView())
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificController") as? GroupSpecificController else {
            return
        }
        
        controller.GroupName = dataGroup.arr![indexPath.row].gname
        controller.day = dataGroup.arr![indexPath.row].gdate
        controller.IdGroup = String(dataGroup.arr![indexPath.row].gid!)
        controller.member = dataGroup.arr![indexPath.row].gmember
       self.present(controller, animated: false, completion: nil)
    }
}
