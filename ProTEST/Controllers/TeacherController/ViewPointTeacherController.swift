//
//  ViewPointTeacherController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/28/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift

class ViewPointTeacherController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewloading: UIView!
    @IBOutlet weak var tbvResult: UITableView!
    
    // MARK: - Variables
    var eid:String!
    let task = SocketIOManager.sharedInstance
    var DataViewPoint = [ArrViewPoint]()
    var qlen = ""
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        ReceiveViewPoint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        task.viewAllPointOfExam(eid, loading,viewloading)
        task.rviewAllPointOfExam(loading, viewloading)
    }
    
    // MARK: - Supporting Methods
    func ReceiveViewPoint() {
        NotificationCenter.default.addObserver(self, selector: #selector(update(notifi:)), name: Notification.Name(rawValue:"rviewAllPointOfExam"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fail), name: Notification.Name(rawValue:"rviewAllPointOfExamF"),object: nil)
    }
    
    @objc func update(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? DataViewPoint{
                    if temp.len == "0" {
                        self.view.makeToast("Chưa ai làm bài thi  ", duration: 2.5, position: .bottom)
                    }
                    DataViewPoint = temp.arr!
                    qlen = temp.qlen!
                } else{
                    print("LOI")
                }
                
            }
        }
    
        tbvResult.reloadData()
    }
    
    @objc func fail(){
        self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
   
    // MARK: - Setup
    func setUp(){
        tbvResult.delegate = self
        tbvResult.dataSource = self
        tbvResult.register(UINib(nibName: "CellViewPoint", bundle: nil), forCellReuseIdentifier: "CellViewPoint")
    }
}

// MARK: - Extensions
extension ViewPointTeacherController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataViewPoint.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellViewPoint") as? CellViewPoint else{
            return UITableViewCell()
        }
        cell.set(DataViewPoint[indexPath.row].user!,"Đúng: " + DataViewPoint[indexPath.row].point! + "/" + qlen)
        return cell
    }
}

extension ViewPointTeacherController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height/14
    }
    
}
