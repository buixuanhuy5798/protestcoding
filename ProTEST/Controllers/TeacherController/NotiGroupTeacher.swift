//
//  NotiGroupTeacher.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/22/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class NotiGroupTeacher: UIViewController {
   
    // MARK: - Outlets
    @IBOutlet weak var tbvNoti: UITableView!
    
    // MARK: - Variables
    var sectionTitle = [String]()
    var rowForSection = [[Member]]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUP()
    }
    
    // MARK: - Setup
    func setUP(){
        tbvNoti.dataSource = self
        tbvNoti.delegate = self
        tbvNoti.register(UINib(nibName: "CellNotiGroup", bundle: nil), forCellReuseIdentifier: "CellNotiGroup")
    }
}

// MARK: - Extensions
extension NotiGroupTeacher: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowForSection[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellNotiGroup") as? CellNotiGroup else {
            return UITableViewCell()
        }
        if sectionTitle[indexPath.section] == "Đang gửi yêu cầu tham gia nhóm" {
            cell.setData(rowForSection[indexPath.section][indexPath.row].user, "2")
        }
        else if sectionTitle[indexPath.section].contains("Đã mời tham gia nhóm")  {
            print(rowForSection[indexPath.section][indexPath.row].user)
            cell.setData(rowForSection[indexPath.section][indexPath.row].user, "3")
        }
        else if sectionTitle[indexPath.section] == "Từ chối tham gia nhóm" {
            cell.setData(rowForSection[indexPath.section][indexPath.row].user, "4")
        }
        else {
            cell.setData(rowForSection[indexPath.section][indexPath.row].user, "5")
        }
        return cell
    }
}

extension NotiGroupTeacher: UITableViewDelegate {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitle[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

