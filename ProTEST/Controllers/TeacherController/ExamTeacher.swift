//
//  ExamTecher.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ExamTeacher: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var lblNameExam: UILabel!
    @IBOutlet weak var tbvEX: UITableView!
    @IBOutlet weak var viewLoading: UIView!
    
    // MARK: - Variables
    var question = [ArrQuestion]()
    var q = [String]()
    var id:String!
    var NameExam:String!
    var answer = [String]()
    var choice = [[String]]()
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loading.startAnimating()
        lblNameExam.text = NameExam
        task.rgetExam(viewLoading, loading)
        setUP()
        ReceiveExamTeacher()
    }
    
    // MARK: Setup
    func setUP(){
        tbvEX.dataSource = self
        tbvEX.delegate = self
        tbvEX.register(UINib(nibName: "CellAnswer", bundle: nil), forCellReuseIdentifier:"CellAnswer")
        tbvEX.register(UINib(nibName: "CellQuestion", bundle: nil), forCellReuseIdentifier: "CellQuestion")
        tbvEX.register(UINib(nibName: "CellChoice", bundle: nil), forCellReuseIdentifier: "CellChoice")
        tbvEX.register(UINib(nibName: "CellQuestion", bundle: nil), forCellReuseIdentifier:"CellQuestion")
        tbvEX.register(UINib(nibName: "CellChoice", bundle: nil), forCellReuseIdentifier:"CellChoice")
    }
    
    // MARK: - Supporting Methods
    func ReceiveExamTeacher() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateExamnoti(notifi:)), name: Notification.Name(rawValue:"getExamTC"),object: nil)
    }
    
    @objc func updateExamnoti(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? DataExam {
                    answer.removeAll()
                    question.removeAll()
                    q.removeAll()
                    answer = temp.aarr ?? [String]()
                    question = temp.qarr ?? [ArrQuestion]()
                    for i in question {
                        q.append(i.q ?? "")
                        choice.append(i.sarr ?? [String]())
                    }
                }
            }
        }
        tbvEX.reloadData()
    }
    
    func getSize(_ string: String) -> CGSize{
        let stringToCalculateSize:String = string
        let stringSize:CGSize = (stringToCalculateSize as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
        
        return CGSize(width: stringSize.width, height: stringSize.height)
        
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnThongke(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = sb.instantiateViewController(withIdentifier: "TabViewPoint") as? UITabBarController else {
            return
        }
       
        let controller0 = controller.viewControllers![0] as! ViewPointTeacherController
        controller0.eid = id
        let controller1 = controller.viewControllers![1] as! Chart1ExamViewPoint
        controller1.eid = id
        self.present(controller, animated: true, completion: nil)
    }
}

// MARK: - Extensions
extension ExamTeacher: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choice[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return q.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellChoice") as? CellChoice else {
            return UITableViewCell()
        }
        cell.setChoice(indexPath.row+1, choice[indexPath.section][indexPath.row])
        return cell
    }
    
}

extension ExamTeacher: UITableViewDelegate{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellQuestion") as? CellQuestion else {
            return UITableViewCell()
        }
        cell.setQuestion(q[section], String(section+1))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let kichthuoc = getSize(q[section])
        if (kichthuoc.width/view.frame.width > 1) {
            return (kichthuoc.width/view.frame.width + 2) * kichthuoc.height + 100
        }
        else {
            return view.frame.height / 10
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellAnswer") as? CellAnswer else {
            return UITableViewCell()
        }
        cell.setAnswer(answer[section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return view.frame.height/19
    }
   
}
