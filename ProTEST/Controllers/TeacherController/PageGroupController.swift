//
//  PageGroupController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Parchment
import KeychainSwift

class PageGroupController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    // MARK: - Variables
    var IdGroup: String!
    var day: String!
    var member: String!
    var first = true
    var control:String?
    var MemberGroup = [Member]()
    var Memberinvited = [Member]()
    var MemberRequest = [Member]()
    var MemberDelete = [Member]()// Minh khong cho vao nhom
    var MemberDeny = [Member]()// Sv khong chap nhan vao nhom
    static var change = false
    let task = SocketIOManager.sharedInstance

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        if PageGroupController.change {
            activity.startAnimating()
            self.task.getInfoOfGroup(IdGroup,activity,viewCover)
            PageGroupController.change = false
        }
        self.task.rgetInfoOfGroup(activity, viewCover)
        ReceiveGroupSpecific()
    }
    
    // MARK: - Setup
    func creatView(_ dataGroup:DataGroupSpecific){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if control == "true"{
            let firstViewController = storyboard.instantiateViewController(withIdentifier: "InfoGroup") as! InfoGroup
            InfoGroup.cuatoi = true
            firstViewController.Day = day
            firstViewController.IDGroup = IdGroup
            firstViewController.Member = "\(MemberGroup.count)"
            firstViewController.nameGroup = dataGroup.gname
            firstViewController.TeacherName = dataGroup.tname
            firstViewController.TeacherUser = dataGroup.tuser
            
            let secondViewController = storyboard.instantiateViewController(withIdentifier: "MemberController") as! MemberController
            secondViewController.TeacherID = dataGroup.tuser
            secondViewController.TeacherName = dataGroup.tname
            secondViewController.MemberGroup = MemberGroup
            secondViewController.MemberRequest = MemberRequest
            secondViewController.Memberinvited = Memberinvited
            secondViewController.idGroup = IdGroup
            
            let thirdViewController = storyboard.instantiateViewController(withIdentifier: "ContestController")
                as! ContestController
            thirdViewController.idGroup = IdGroup
            
            let fouthViewController = storyboard.instantiateViewController(withIdentifier: "NotiGroupTeacher") as! NotiGroupTeacher
            fouthViewController.sectionTitle.removeAll()
            fouthViewController.rowForSection.removeAll()
            
            if MemberRequest.count > 0 {
                fouthViewController.rowForSection.append(MemberRequest)
                fouthViewController.sectionTitle.append("Đang gửi yêu cầu tham gia nhóm")
            }
            if Memberinvited.count > 0 {
                fouthViewController.rowForSection.append(Memberinvited)
                fouthViewController.sectionTitle.append("Đã mời tham gia nhóm")
            }
            if MemberDeny.count > 0 {
                fouthViewController.rowForSection.append(MemberDeny)
                fouthViewController.sectionTitle.append("Từ chối tham gia nhóm")
            }
            if MemberDelete.count > 0 {
                fouthViewController.rowForSection.append(MemberDelete)
                fouthViewController.sectionTitle.append("Không được chấp nhận vào nhóm")
            }
            
            let pagingViewController = FixedPagingViewController(viewControllers: [
                firstViewController,
                secondViewController,
                thirdViewController,
                fouthViewController
                ])
            
            pagingViewController.indicatorColor = #colorLiteral(red: 0.2517958581, green: 0.7541208863, blue: 0.5185554028, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.frame.width/4, height: view.bounds.height/15
            
            )
            
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
        }
        else {
            InfoGroup.cuatoi = false
            
            let firstViewController = storyboard.instantiateViewController(withIdentifier: "InfoGroup") as! InfoGroup
            firstViewController.Day = day
            firstViewController.IDGroup = IdGroup
            firstViewController.Member = "\(MemberGroup.count)"
            firstViewController.nameGroup = dataGroup.gname
            firstViewController.TeacherName = dataGroup.tname
            firstViewController.TeacherUser = dataGroup.tuser
            
            let pagingViewController = FixedPagingViewController(viewControllers: [firstViewController])
            pagingViewController.indicatorColor = #colorLiteral(red: 0.2517958581, green: 0.7541208863, blue: 0.5185554028, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.bounds.width, height: 0)
            
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
        }
    }
    
    // MARK: - Supporting Methods
    func ReceiveGroupSpecific() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroupSpecific(notifi:)), name: Notification.Name(rawValue:"InfoGroupSpecific"),object: nil)
    }
    
    @objc func updateGroupSpecific(notifi : Notification) {
       if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                Memberinvited.removeAll()
                MemberGroup.removeAll()
                MemberRequest.removeAll()
                MemberDelete.removeAll()
                MemberDeny.removeAll()
                
                let temp = msg as! DataGroupSpecific
                if temp.control == true {
                    control = "true"
                }
                else {
                    control = "false"
                }
                
                for i in temp.arr! {
                    if i.status == "1" {
                        let x = Member(i.name!, i.user!)
                        MemberGroup.append(x)
                    }
                    else if i.status == "2" {
                        let x = Member(i.name!,i.user!)
                        MemberRequest.append(x)
                    }
                    else if i.status == "3" {
                        let x = Member(i.name!,i.user!)
                        Memberinvited.append(x)
                    }
                    else if i.status == "4" {
                        let x = Member(i.name!,i.user!)
                        MemberDeny.append(x)
                    }
                    else if i.status == "5" {
                        let x = Member(i.name!,i.user!)
                        MemberDelete.append(x)
                    }
                }
                creatView(temp)
            }
        }
    }
}
