//
//  GroupSpecificController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/5/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Parchment

class GroupSpecificController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var UIVIEW: UIView!
    
    // MARK: - Variables
    var IdGroup: String!
    var day: String!
    var member: String!
    var GroupName:String!
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        lblGroupName.text = GroupName
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PageGroupController") as! PageGroupController
   
        viewController.day = day
        viewController.IdGroup = IdGroup
        viewController.member = member
        viewController.view.frame = CGRect(x: 0, y: 0, width: UIVIEW.frame.width, height: UIVIEW.frame.height)
      
        self.addChild(viewController)
        UIVIEW.addSubview(viewController.view)
        ReceiveUpdateName()

    }
    
    // MARK: - Supporting Methods
    func ReceiveUpdateName(){
         NotificationCenter.default.addObserver(self, selector: #selector(updateName(notifi:)), name: Notification.Name(rawValue:"UpdateName"),object: nil)
    }
    
    @objc func updateName(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
              
                let temp = msg as? String
          
                lblGroupName.text = temp
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        GroupController.change = true
        dismiss(animated: false, completion: nil)
    }
    
}
