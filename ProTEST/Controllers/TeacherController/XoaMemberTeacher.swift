//
//  XoaMemberTeacher.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import SCLAlertView

class XoaMemberTeacher: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var tbvSearch: UITableView!
    
    // MARK: - Variables
    var MemberGroup = [Member]()
    var MemberSearch = [Member]()
    var idGroup:String!
    var tuser:String!
    var index:Int!
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUP()
        DeleteSuccessOrFail()
    }
    
    // MARK: - Setup
    func setUP(){
        MemberSearch = MemberGroup
        tbvSearch.dataSource = self
        tbvSearch.delegate = self
        SearchBar.delegate = self
        tbvSearch.register(UINib(nibName:"MemberDelete",bundle:nil), forCellReuseIdentifier: "MemberDelete")
        
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        PageGroupController.change = true
        dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Supporting Methods
    func DeleteSuccessOrFail() {
        NotificationCenter.default.addObserver(self, selector: #selector(delete(notifi:)), name: Notification.Name(rawValue: "DeleteMemberSuccess"), object: nil)
    }
    
    @objc func delete(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let res = msg as! String
                if res == "0" {
                    self.view.makeToast("Đã xoá thành công", duration: 1.0, position: .center)
                    MemberSearch.remove(at: index)
                    tbvSearch.reloadData()
                } else {
                    self.view.makeToast("Tác vụ thất bại", duration: 1.0, position: .center)
                }
            }
        }
    }
    
    @objc func buttonDelete(sender: UIButton){
        index = sender.tag
        task.groupDeleteOrRefuseStudentByTeacher(idGroup, tuser, MemberSearch[sender.tag].user, loading, viewLoading)
        task.rgroupDeleteOrRefuseStudentByTeacher(loading, viewLoading)
    }
}

// MARK: - Extensions
extension XoaMemberTeacher: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {MemberSearch = MemberGroup;
            tbvSearch.reloadData()
            return
        }
        MemberSearch = MemberGroup.filter({ member -> Bool in
            guard let text = SearchBar.text else {
                return false
            }
            return member.name?.lowercased().range(of: text.lowercased()) != nil
        })
        tbvSearch.reloadData()
    }
}

extension XoaMemberTeacher: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MemberSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tbvSearch.dequeueReusableCell(withIdentifier: "MemberDelete") as? MemberDelete
        else {
            return UITableViewCell()
        }
        cell.setMember(MemberSearch[indexPath.row].name, MemberSearch[indexPath.row].user)
        cell.btnCheckOL.tag = indexPath.row
        cell.btnCheckOL.addTarget(self, action: #selector(buttonDelete), for: .touchUpInside)
        return cell
    }
    
}

extension XoaMemberTeacher:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height / 14
    }
}
