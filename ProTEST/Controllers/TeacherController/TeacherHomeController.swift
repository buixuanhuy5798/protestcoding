//
//  TeacherHomeController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 10/30/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift
import QRCoder
import KeychainSwift

class TeacherHomeController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var QRCode: UIImageView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var btnCTNhom: UIButton!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var btnCTDe: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var lblGroup: UILabel!
    @IBOutlet weak var lblExam: UILabel!
    @IBOutlet weak var btnDangXuat: UIButton!
    
    // MARK: - Variables
    var id: String!
    var sode: Int!
    var info: DataLogin!
    var groupDT:DataGroup!
    let a = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDangXuat.layer.cornerRadius = 10
        btnDangXuat.clipsToBounds = true
        setupView()
        setData()
    }
    
    // MARK: - Actions
    @IBAction func ShowDetailGroup(_ sender: Any) {
        guard let controller = tabBarController?.viewControllers![1] as? GroupController else {
            return
        }
        tabBarController?.selectedViewController = controller
    }
    
    @IBAction func btnExam(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "AllExamTeacherController") as? AllExamTeacherController else {
            return
        }
        self.task.getInfoAllExamTeacherMake(lblID.text!)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func Sign_Out(_ sender: Any) {
        SocketIOManager.firstChange = true
        SocketIOManager.firstChange2 = true
        LoginController.user = nil
        a.delete("teacher"); a.delete("username")
        GroupController.change = false
        InfoGroup.cuatoi = true
        PageGroupController.change = false
        StudentGroupController.change = false
        StudentContestController.timeInListExam = true
        LoginController.teacherLogin = false
        LoginController.studentLogin = false
        task.closeConnection()
        self.dismiss(animated: false, completion: nil)
    }
    

    // MARK: - Setups
    func setupView(){
        imageAvatar.clipsToBounds = true
        btnCTDe.layer.cornerRadius = 10
        btnCTNhom.layer.cornerRadius = 10
        viewInfo.layer.cornerRadius = 17
        
    }
    
    func setData(){
        let generator = QRCodeGenerator()
        QRCode.image = generator.createImage(value: "GV" + id,size: QRCode.frame.size)
        lblGroup.text = groupDT.len
        lblID.text = id
        lblName.text = info.name
        lblPhone.text = info.phone
        lblExam.text = info.elen
    }
}

