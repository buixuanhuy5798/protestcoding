//
//  Testing.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/21/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import SocketIO
import KeychainSwift

class GroupSearchInfo: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewloading: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var tbvGroup: UITableView!
    
    // MARK: - Variables
    var gv:Bool!
    var data = [ArrGroup]()
    var id:String!
    let task = SocketIOManager.sharedInstance
    let a = KeychainSwift()
    var socket: SocketIOClient?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        setup()
        super.viewDidLoad()
        if gv {
            task.GroupSearchInfo(0, id, loading, viewloading)
            task.rGroupSearchInfo(0, loading, viewloading)
        }
        else {
            task.GroupSearchInfo(1, id, loading, viewloading)
            task.rGroupSearchInfo(1, loading, viewloading)
        }
        listenNoti()
    }
    
    // MARK: - Setup
    func setup(){
        tbvGroup.delegate = self
        tbvGroup.dataSource = self
        tbvGroup.register(UINib(nibName: "GroupCellTeacher", bundle: nil), forCellReuseIdentifier: "GroupCellTeacher")
    }

    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func listenNoti(){
        NotificationCenter.default.addObserver(self, selector: #selector(listen(notifi:)), name: Notification.Name(rawValue:"GroupSearchInfoSV"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(listen(notifi:)), name: Notification.Name(rawValue:"GroupSearchInfoTC"),object: nil)
    }
    
    @objc func listen(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let response = msg as! [ArrGroup]
                data = response
                tbvGroup.reloadData()
            }
        }
    }
}

// MARK: - Extensions
extension GroupSearchInfo: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCellTeacher") as? GroupCellTeacher else {
            return UITableViewCell()
        }
        cell.set(data[indexPath.row].gname, data[indexPath.row].gid, data[indexPath.row].gmember, "")
        return cell
    }
    
    
}
extension GroupSearchInfo: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if a.getBool("teacher")!{
        } else {
            task.getInfoOfGroup(String(data[indexPath.row].gid!), UIActivityIndicatorView(),UIView())
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificControllerStudent") as? GroupSpecificControllerStudent else {
                return
            }
            controller.groupName = data[indexPath.row].gname
            controller.status = "0"
            controller.IdGroup = String(data[indexPath.row].gid!)
            controller.member = data[indexPath.row].gmember
            self.present(controller, animated: true, completion: nil)
        }
    }
}
