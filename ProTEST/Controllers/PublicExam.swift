//
//  PublicExam.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/28/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class PublicExam: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewloading: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var tbvExam: UITableView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var DataExam = [ArrContest]()
    var tuser:String!

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        task.getDeChonLoc(tuser, loading, viewloading)
        task.rgetDeChonLoc(loading, viewloading)
        setData()
        ReceiveAllExamTeacherMake()
    }
    
    // MARK: - Setup
    func setData(){
        tbvExam.dataSource = self
        tbvExam.delegate = self
        tbvExam.register(UINib(nibName:"ExamTeacherMake",bundle:nil), forCellReuseIdentifier: "ExamTeacherMake")
    }
    
    // MARK: - Supporting Methods
    func ReceiveAllExamTeacherMake() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateExamTCMake(notifi:)), name: Notification.Name(rawValue:"rgetDeChonLoc"),object: nil)
    }

    @objc func updateExamTCMake(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                DataExam.removeAll()
                if let temp = msg as? [ArrContest]{
                    for i in temp {
                        if i.publish == true {
                            DataExam.append(i)
                        }
                    }
                } else{
                    print("LOI")
                }
            }
        }
        tbvExam.reloadData()
    }

    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Extensions
extension PublicExam: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataExam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExamTeacherMake") as? ExamTeacherMake else {
            return UITableViewCell()
        }
        cell.btnChitiet.tag = indexPath.row
        cell.setCell(DataExam[indexPath.row].name, DataExam[indexPath.row].created, DataExam[indexPath.row].type, DataExam[indexPath.row].qlen, DataExam[indexPath.row].publish, DataExam[indexPath.row].eid, DataExam[indexPath.row].tuser, DataExam[indexPath.row].timeStart, DataExam[indexPath.row].timeEnd)
        return cell
    }
}

extension PublicExam: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}

