//
//  MemberStudentController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/18/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class MemberStudentController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblTeacherID: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewMember: UITableView!
    @IBOutlet weak var teacherView: UIView!
    
    // MARK: - Variables
    var teacherName: String?
    var teacherID: String?
    var MemberGroup = [Member]()
    var Memberinvited = [Member]()
    var MemberRequest = [Member]()
    var currentMemberSearching = [Member]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        setUpSearchBar()
    }
    
    // MARK: - Setup
    func setUpTableView(){
        lblTeacherName.text = teacherName
        lblTeacherID.text = teacherID
        tableViewMember.register(UINib(nibName: "MemberCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        tableViewMember.dataSource = self
        tableViewMember.delegate = self
        currentMemberSearching = MemberGroup
        tableViewMember.reloadData()
    }
    
    func setUpSearchBar() {
        searchBar.delegate = self
    }
    
}

// MARK: - Extensions
extension MemberStudentController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentMemberSearching.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as? MemberCell else {
            return UITableViewCell()
        }
        cell.setMember(currentMemberSearching[indexPath.row].name, currentMemberSearching[indexPath.row].user)
        return cell
    }
}

extension MemberStudentController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return teacherView.frame.height
    }
}

extension MemberStudentController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentMemberSearching = MemberGroup
            tableViewMember.reloadData()
            return
        }
        currentMemberSearching = MemberGroup.filter({ (member) -> Bool in
            member.name.lowercased().contains(searchText.lowercased())
        })
        tableViewMember.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.placeholder = "Nhập thông tin sinh viên"
        searchBar.text = nil
        searchBar.endEditing(true)
    }
}
