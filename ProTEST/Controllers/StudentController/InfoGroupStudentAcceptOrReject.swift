//
//  InfoGroupStudentAcceptOrReject.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/19/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift
import SCLAlertView
import KeychainSwift
import QRCoder
class InfoGroupStudentAcceptOrReject: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var QRimage: UIImageView!
    @IBOutlet weak var lblIDNhom: UILabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblIDTeacher: UILabel!
    @IBOutlet weak var lblNumberOfMem: UILabel!
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    // MARK: - Variables
    let keychain = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    var IDNhom: String?
    var teacherName: String?
    var IDTeacher: String?
    var numberOfMem: String?
    var IDUser: String?
    var groupName:String?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfoFromServer()
        viewCover.alpha = 0.5
        loading.isHidden = true
        viewCover.isHidden = true
    }
    
    // MARK: - Setup
    func getInfoFromServer() {
        let generator = QRCodeGenerator()
        QRimage.image = generator.createImage(value: "GR\n" + IDNhom! + "\n" + groupName! + "\n" + numberOfMem!, size: QRimage.frame.size)
        lblIDNhom.text = IDNhom
        lblTeacherName.text = teacherName
        lblIDTeacher.text = IDTeacher
        lblNumberOfMem.text = numberOfMem
    }
    
    // MARK: - Actions
    @IBAction func AcceptToJoin(_ sender: Any) {
        StudentGroupController.change = true
        self.task.studentJoinOrAcceptGroup(self.IDNhom, self.keychain.get("username"), viewCover, loading)
        self.task.rstudentJoinOrAcceptGroup(loading, viewCover)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inGroupS(notifi:)), name: Notification.Name(rawValue:"rstudentJoinOrAcceptGroupSuccess"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inGroupF(notifi:)), name: Notification.Name(rawValue:"rstudentJoinOrAcceptGroupFail"),object: nil)
    }
    
    @IBAction func RejectToJoin(_ sender: Any) {
        StudentGroupController.change = true
        self.task.studentExitOrRefuseGroup(self.IDNhom, self.keychain.get("username"), viewCover, loading)
        self.task.rstudentExitOrRefuseGroup(loading, viewCover)
        NotificationCenter.default.addObserver(self, selector: #selector(self.outGroupS(notifi:)), name: Notification.Name(rawValue:"rstudentExitOrRefuseGroupSuccess"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.outGroupF(notifi:)), name: Notification.Name(rawValue:"rstudentExitOrRefuseGroupFail"),object: nil)
    }
    
    // MARK: - Others
    @objc func outGroupS(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func outGroupF(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
            }
        }
    }
    
    @objc func inGroupS(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @objc func inGroupF(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
            }
        }
    }
}
