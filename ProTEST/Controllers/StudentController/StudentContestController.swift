//
//  StudentContestController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/19/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import KeychainSwift
import SCLAlertView
import SocketIO
import Toast_Swift

class StudentContestController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableViewListExam: UITableView!
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var lblTimeLeft: UILabel!
    @IBOutlet weak var viewThongBao: UIView!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewCoverThongBao: UIView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    let keychain = KeychainSwift()
    var idGroup: String?
    var DuLieuBaiTest: DataExam?
    var listExam = [ArrContest]()
    static var timeInListExam = true // = false nếu chuyển sang màn LàmBài
    let TimeLeft = DispatchQueue.init(label: "timeLeft")
    var timeLeft: String?
    var thoiGianBatDau: Int?
    var thoiGianKetThuc: Int?
    var chiSoMang: Int?
    var lamBaiThu = false
    var sendIDExam = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCoverThongBao.isHidden = true
        viewThongBao.frame = CGRect(x: 0, y: view.bounds.height, width: view.bounds.width, height: view.frame.height/5)
        lblTimeLeft.frame = CGRect(x: self.viewThongBao.bounds.midX - self.viewThongBao.bounds.width / 2, y: 10, width: self.viewThongBao.bounds.width, height: self.viewThongBao.bounds.height / 3)
        btnStart.frame = CGRect(x: 0, y: self.lblTimeLeft.bounds.origin.y + self.lblTimeLeft.bounds.height + 10 , width: self.viewThongBao.bounds.width / 2 - 2.5, height: self.view.bounds.height * 0.07)
        btnCancel.frame = CGRect(x: self.viewThongBao.bounds.width / 2 + 2.5, y: self.lblTimeLeft.bounds.origin.y + self.lblTimeLeft.bounds.height + 10, width: self.btnStart.bounds.width, height: self.btnStart.bounds.height)
        setUpTableViewListExams()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lamBaiThu = false
        sendIDExam = false
        self.task.getInfoAllExamAcceptForGroup(idGroup, activity, viewCover)
        self.task.rgetInfoAllExamAcceptForGroup(activity, viewCover)
        getAllDataForListExams()
    }
    
    // MARK: - Setup
    func setUpTableViewListExams() {
        tableViewListExam.register(UINib(nibName: "ExamTeacherMake", bundle: nil), forCellReuseIdentifier: "ExamTeacherMake")
        tableViewListExam.dataSource = self
        tableViewListExam.delegate = self
    }
    
    // MARK: - Supporting Methods
    func getBlockChainTime() {
        self.task.getTimeStamp()
        self.task.rgetTimeStamp()
    }
    
    func GetTimeDoingTest(_ blockchainTime: Int?) -> String? {
        var thoiGianLamBai: String?
        
        let s = thoiGianKetThuc
        let e = blockchainTime
        
        let timeConLai = s! - e!
        
        let hour = Int(timeConLai)/3600; let minute = (Int(timeConLai)%3600)/60; let second = (Int(timeConLai)%3600)%60
        
        if String(hour).count == 1 {
            if String(minute).count == 1 {
                if String(second).count == 1 {
                    thoiGianLamBai = "0" + String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
                } else {
                    thoiGianLamBai = "0" + String(hour) + ":" + "0" + String(minute) + ":" + String(second)
                }
            } else if String(second).count == 1 {
                thoiGianLamBai = "0" + String(hour) + ":" + String(minute) + ":" + "0" + String(second)
            } else {
                thoiGianLamBai = "0" + String(hour) + ":" + String(minute) + ":" + String(second)
            }
        } else if String(minute).count == 1 {
            if String(second).count == 1 {
                thoiGianLamBai = String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
            } else {
                thoiGianLamBai = String(hour) + ":" + "0" + String(minute) + ":" + String(second)
            }
        } else {
            if String(second).count == 1 {
                thoiGianLamBai = String(hour) + ":" + String(minute) + ":" + "0" + String(second)
            } else {
                thoiGianLamBai = String(hour) + ":" + String(minute) + ":" + String(second)
            }
        }
        return thoiGianLamBai
    }
    
    // MARK: - Actions
    @IBAction func actionStartTest(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "LamBai") as? LamBai else {
            return
        }
        controller.DuLieuBaiTest = DuLieuBaiTest
        StudentContestController.timeInListExam = false
        controller.lamLai = false
        self.present(controller, animated: false, completion: nil)
        self.viewCoverThongBao.isHidden = true
        UIView.animate(withDuration: 0.1) {
            self.viewThongBao.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.frame.height/5)
        }
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.viewCoverThongBao.isHidden = true
        UIView.animate(withDuration: 0.1) {
            self.viewThongBao.frame = CGRect(x: 0, y: self.view.bounds.height, width: self.view.bounds.width, height: self.view.frame.height/5)
        }
    }
    
    // MARK: - Nhan Du Lieu
    func getAllDataForListExams() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoadDataContestS(notifi:)), name: Notification.Name(rawValue:"rgetInfoAllExamAcceptForGroupSuccess"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoadDataContestF(notifi:)), name: Notification.Name(rawValue:"rgetInfoAllExamAcceptForGroupFail"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StartTest(notifi:)), name: Notification.Name(rawValue:"getExamTC"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getTimeLeft(notifi:)), name: Notification.Name(rawValue:"rgetTimeStampPrepare"),object: nil)
    }
    
    @objc func StartTest(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                DuLieuBaiTest = msg as? DataExam
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "LamBai") as? LamBai else {
                    return
                }
                controller.DuLieuBaiTest = msg as? DataExam
                if !self.lamBaiThu {
                    self.viewCoverThongBao.isHidden = false
                    UIView.animate(withDuration: 0.1, animations: {
                        self.viewThongBao.frame = CGRect(x: 0, y: 4/5 * self.view.bounds.height, width: self.view.bounds.width, height: self.view.bounds.height/5)
                    })
                    TimeLeft.async {
                        while true {
                            sleep(1)
                            self.getBlockChainTime()
                            DispatchQueue.main.async {
                                self.lblTimeLeft.text = "Bạn có \(self.timeLeft!) thời gian để làm.\nBạn đã sẵn sàng làm bài?"
                            }
                        }
                    }
                } else {
                    let alert = SCLAlertView()
                    alert.addButton("OK") {
                        controller.lamLai = true
                        StudentContestController.timeInListExam = false
                        self.present(controller, animated: false, completion: nil)
                    }
                    alert.showTitle("Làm bài", subTitle: "Đã quá thời gian làm bài.\nBạn vẫn có thể làm, nhưng điểm bài thi sẽ không được lưu. Sẵn sàng?" , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
                }
            }
        }
    }
    
    @objc func LoadDataContestS(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                listExam.removeAll()
                if let temp = msg as? [ArrContest] {
                    listExam = temp
                    tableViewListExam.reloadData()
                }
            }
        }
    }
    
    @objc func LoadDataContestF(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                self.view.makeToast(msg as? String, duration: 2.0, position: .center)
            }
        }
    }
    
    @objc func buttonStart(sender: UIButton) {
        self.lamBaiThu = false
        self.sendIDExam = false
        viewCover.isHidden = false
        activity.isHidden = false
        activity.startAnimating()
        thoiGianBatDau = Int(listExam[sender.tag].timeStart!)
        thoiGianKetThuc = Int(listExam[sender.tag].timeEnd!)
        chiSoMang = sender.tag
        self.getBlockChainTime()
        
    }
    
    @objc func getTimeLeft(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if thoiGianBatDau! > msg as! Int {
                    activity.isHidden = true
                    viewCover.isHidden = true
                    if !self.sendIDExam {
                        self.sendIDExam = true
                        self.view.makeToast("Chưa tới thời gian làm bài", duration: 3.0, position: .center)
                    }
                } else if thoiGianKetThuc! >= msg as! Int {
                    let blockchainTime = msg as? Int
                    self.timeLeft = self.GetTimeDoingTest(blockchainTime)
                    if !self.sendIDExam {
                        self.sendIDExam = true
                        self.task.getExam(self.listExam[self.chiSoMang!].eid!)
                        self.task.rgetExam(self.viewCover, self.activity)
                    }
                } else {
                    self.lamBaiThu = true
                    let blockchainTime = msg as? Int
                    self.timeLeft = self.GetTimeDoingTest(blockchainTime)
                    if !sendIDExam {
                        self.sendIDExam = true
                        self.task.getExam(self.listExam[self.chiSoMang!].eid!)
                        self.task.rgetExam(self.viewCover, self.activity)
                    }
                }
            }
        }
    }
}

// MARK: - Extensions
extension StudentContestController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listExam.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExamTeacherMake") as? ExamTeacherMake else {
            return UITableViewCell()
        }
        if listExam[indexPath.row].timeStart == "" || listExam[indexPath.row].timeEnd == "" {
            cell.btnChitiet.isHidden = true
            
        } else {
            cell.btnChitiet.setTitle("Làm bài", for: .normal)
            cell.btnChitiet.backgroundColor = #colorLiteral(red: 0.3624967635, green: 0.739374578, blue: 0.3061080873, alpha: 1)
            cell.btnChitiet.tag = indexPath.row
            cell.btnChitiet.addTarget(self, action: #selector(buttonStart), for: .touchUpInside)
        }
        cell.setCell(listExam[indexPath.row].name, listExam[indexPath.row].created, listExam[indexPath.row].type, listExam[indexPath.row].qlen, listExam[indexPath.row].publish, listExam[indexPath.row].eid, listExam[indexPath.row].tuser, listExam[indexPath.row].timeStart, listExam[indexPath.row].timeEnd)
        return cell
    }
    
    
}

extension StudentContestController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
