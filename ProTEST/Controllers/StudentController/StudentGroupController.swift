//
//  StudentGroupController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import KeychainSwift

class StudentGroupController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblViewGroupJoin: UITableView!
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    // MARK: - Variables
    let keychain = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    var allGroup: ResponseGroup?
    static var change = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCover.isHidden = true
        activity.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if StudentGroupController.change {
            self.task.getInfoAllGroupStudentJoin(keychain.get("username"), activity)
            viewCover.isHidden = false
        }
        self.task.rgetInfoAllGroupStudentJoin(activity)
        NotificationCenter.default.addObserver(self, selector: #selector(LoadData(notifi:)), name: Notification.Name(rawValue:"InfoAllGroupStudentJoin_Change"),object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupTableView()
        if Int((allGroup?.data?.len)!)! == 0 {
            self.view.makeToast("Bạn đang không ở trong nhóm nào", duration: 2.0, position: .center)
        }
    }
    
    // MARK: - Setup
    func setupTableView() {
        tblViewGroupJoin.register(UINib(nibName: "GroupCellStudent", bundle: nil), forCellReuseIdentifier: "GroupCellStudent")
        tblViewGroupJoin.dataSource = self
        tblViewGroupJoin.delegate = self
        tblViewGroupJoin.reloadData()
    }
    
    // MARK: - Others
    @objc func LoadData(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                allGroup = msg as? ResponseGroup
                tblViewGroupJoin.reloadData()
                viewCover.isHidden = true
                activity.stopAnimating()
                activity.isHidden = true
                StudentGroupController.change = false
            }
        }
    }
}

// MARK: - Extensions
extension StudentGroupController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int((allGroup?.data?.len)!)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCellStudent") as? GroupCellStudent else {
            return UITableViewCell()
        }
        cell.setDataForCell( allGroup?.data?.arr?[indexPath.row].gname, allGroup?.data?.arr?[indexPath.row].gid, allGroup?.data?.arr?[indexPath.row].gmember, allGroup?.data?.arr?[indexPath.row].tname, allGroup?.data?.arr?[indexPath.row].status)
        return cell
    }
}

extension StudentGroupController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        task.getInfoOfGroup(String((allGroup?.data?.arr![indexPath.row].gid)!), UIActivityIndicatorView(),UIView())
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "GroupSpecificControllerStudent") as? GroupSpecificControllerStudent else {
            return
        }
        controller.groupName = allGroup?.data?.arr![indexPath.row].gname
        controller.status = allGroup?.data?.arr![indexPath.row].status
        controller.IdGroup = String((allGroup?.data?.arr![indexPath.row].gid)!)
        controller.member = allGroup?.data?.arr![indexPath.row].gmember
        self.present(controller, animated: true, completion: nil)
    }
}
