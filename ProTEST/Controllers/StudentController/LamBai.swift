//
//  LamBai.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/20/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import SCLAlertView
import Toast_Swift

class LamBai: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableViewQuestion: UITableView!
    @IBOutlet weak var lblTenDe: UILabel!
    @IBOutlet weak var lblThoiGian: UILabel!
    @IBOutlet weak var btnPreviousQuestion: UIButton!
    @IBOutlet weak var currentQuestion: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var DuLieuBaiTest: DataExam?
    var sttCauHoi = 1
    var yourAnswer = [Int]()
    var choice = [[String]]()
    var question = [String]()
    var listQuestion = [ArrQuestion]()
    var hetGio = false
    var setYourChoice = [[Bool]]()
    var unchecked = [Bool]()
    var submit = false
    var daNopBai = false
    var blockchainTime: Int?
    var timeKetThuc: String?
    var lamLai: Bool?
    var timeConLai: Int?
    let TimeThread = DispatchQueue(label: "TimeThread")
    
    // chưa dùng thời gian hệ thống
    
    /* so sánh thời gian bắt đầu bài với thời gian hệ thống
        nếu timeStart > timeStamp => không cho làm
 
    */

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if lamLai! {
            let hihi = Int((DuLieuBaiTest?.timeEnd)!)! - Int((DuLieuBaiTest?.timeStart)!)!
            timeConLai = hihi
            let hour = hihi/3600; let minute = (hihi%3600)/60; let second = (hihi%3600)%60
            
            if String(hour).count == 1 {
                if String(minute).count == 1 {
                    if String(second).count == 1 {
                        lblThoiGian.text = "0" + String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
                    } else {
                        lblThoiGian.text = "0" + String(hour) + ":" + "0" + String(minute) + ":" + String(second)
                    }
                } else if String(second).count == 1 {
                    lblThoiGian.text = "0" + String(hour) + ":" + String(minute) + ":" + "0" + String(second)
                } else {
                    lblThoiGian.text = "0" + String(hour) + ":" + String(minute) + ":" + String(second)
                }
            } else if String(minute).count == 1 {
                if String(second).count == 1 {
                    lblThoiGian.text = String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
                } else {
                    lblThoiGian.text = String(hour) + ":" + "0" + String(minute) + ":" + String(second)
                }
            } else {
                if String(second).count == 1 {
                    lblThoiGian.text = String(hour) + ":" + String(minute) + ":" + "0" + String(second)
                } else {
                    lblThoiGian.text = String(hour) + ":" + String(minute) + ":" + String(second)
                }
            }
        }
        lblTenDe.text = DuLieuBaiTest?.name
        timeKetThuc = DuLieuBaiTest?.timeEnd
        btnSubmit.layer.cornerRadius = 12
        btnSubmit.clipsToBounds = true
        parseToArray()
        setUpTableQuestion()
        currentQuestion.text = "Câu 1 / \(listQuestion.count)"
        TimeThread.async {
            while true {
                sleep(1)
                DispatchQueue.main.async {
                    self.getBlockchainTime()
                }
                if self.hetGio || self.submit {
                    break
                }
            }
            DispatchQueue.main.async {
                self.OutOfTime()
                self.lblThoiGian.text = "00:00:00"
            }
        }
    }
    
    // MARK: - Setup
    func setUpTableQuestion() {
        tableViewQuestion.register(UINib(nibName: "CellQuestion", bundle: nil), forCellReuseIdentifier: "CellQuestion")
        tableViewQuestion.register(UINib(nibName: "CellAnswerForStudent", bundle: nil), forCellReuseIdentifier: "CellAnswerForStudent")
        tableViewQuestion.dataSource = self
        tableViewQuestion.delegate = self
        tableViewQuestion.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.LayTime(notifi:)), name: Notification.Name(rawValue:"rgetTimeStamp"),object: nil)
    }
    
    func parseToArray() {
        listQuestion = (DuLieuBaiTest?.qarr)!
        for q in listQuestion {
            question.append(q.q!)
            choice.append(q.sarr!)
            yourAnswer.append(-1)
            for _ in 0..<Int(q.slen!)! {
                unchecked.append(false)
            }
            setYourChoice.append(unchecked)
            unchecked.removeAll()
        }
    }
    
    // MARK: - Supporting Methods
    func OutOfTime() {
        self.hetGio = true
        self.daNopBai = true
        self.view.makeToast("Đã nộp bài", duration: 1.0, position: .center)
        btnSubmit.isHidden = true
        self.task.autoMask(self.listQuestion.count, self.yourAnswer, Int((self.DuLieuBaiTest?.eid)!)!)
        self.task.rautoMask()
        NotificationCenter.default.addObserver(self, selector: #selector(self.LayDiem(notifi:)), name: Notification.Name(rawValue:"getMark"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.LayDiemFail(notifi:)), name: Notification.Name(rawValue:"getMarkFail"),object: nil)
        tableViewQuestion.allowsSelection = false
        yourAnswer.removeAll()
    }
    
    func getBlockchainTime() {
        self.task.getTimeStamp()
        self.task.rgetTimeStamp()
    }
    
    func SubTime() {
        var s = Int(timeKetThuc!)!
        var e = blockchainTime!
        if lamLai! {
            s = timeConLai!
            e = 1
            if timeConLai! == 0 {
                self.view.makeToast("Đã hết giờ làm bài", duration: 1.0, position: .center)
                self.hetGio = true
                self.submit = true
                return
            } else {
                timeConLai = s - e
            }
        } else {
            if e == s {
                self.view.makeToast("Đã hết giờ làm bài", duration: 1.0, position: .center)
                self.hetGio = true
                self.submit = true
                return
            } else {
                timeConLai = s - e
            }
        }
        
        let hour = Int(timeConLai!)/3600; let minute = (Int(timeConLai!)%3600)/60; let second = (Int(timeConLai!)%3600)%60
        
        if String(hour).count == 1 {
            if String(minute).count == 1 {
                if String(second).count == 1 {
                    lblThoiGian.text = "0" + String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
                } else {
                    lblThoiGian.text = "0" + String(hour) + ":" + "0" + String(minute) + ":" + String(second)
                }
            } else if String(second).count == 1 {
                lblThoiGian.text = "0" + String(hour) + ":" + String(minute) + ":" + "0" + String(second)
            } else {
                lblThoiGian.text = "0" + String(hour) + ":" + String(minute) + ":" + String(second)
            }
        } else if String(minute).count == 1 {
            if String(second).count == 1 {
                lblThoiGian.text = String(hour) + ":" + "0" + String(minute) + ":" + "0" + String(second)
            } else {
                lblThoiGian.text = String(hour) + ":" + "0" + String(minute) + ":" + String(second)
            }
        } else {
            if String(second).count == 1 {
                lblThoiGian.text = String(hour) + ":" + String(minute) + ":" + "0" + String(second)
            } else {
                lblThoiGian.text = String(hour) + ":" + String(minute) + ":" + String(second)
            }
        }
    }
    
    func getSize(_ string: String) -> CGSize{
        let stringToCalculateSize:String = string
        let stringSize:CGSize = (stringToCalculateSize as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14.0)])
        
        return CGSize(width: stringSize.width, height: stringSize.height)
    }
    
    // MARK: - Actions
    @IBAction func Action_Back(_ sender: Any) {
        if self.daNopBai {
            StudentContestController.timeInListExam = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let controller = storyboard.instantiateViewController(withIdentifier: "StudentContestController") as? StudentContestController else {
                return
            }
            controller.lamBaiThu = false
            controller.sendIDExam = false
            self.dismiss(animated: true, completion: nil)
        } else {
            let alert = SCLAlertView()
            alert.addButton("OK") {
                StudentContestController.timeInListExam = true
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "StudentContestController") as? StudentContestController else {
                    return
                }
                controller.lamBaiThu = false
                controller.sendIDExam = false
                self.dismiss(animated: true, completion: nil)
            }
            alert.showTitle("Thoát", subTitle: "Bạn có chắc chắn muốn thoát?" , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
        }
    }
    
    @IBAction func QuayVeCauTruoc(_ sender: Any) {
        if sttCauHoi > 1 {
            sttCauHoi -= 1
            currentQuestion.text = "Câu \(sttCauHoi) / \(listQuestion.count)"
            tableViewQuestion.reloadData()
        }
    }
    
    @IBAction func CauTiep(_ sender: Any) {
        if sttCauHoi < Int((DuLieuBaiTest?.qlen)!)! {
            sttCauHoi += 1
            currentQuestion.text = "Câu \(sttCauHoi) / \(listQuestion.count)"
            tableViewQuestion.reloadData()
        }
    }
    
    @IBAction func Submit(_ sender: Any) {
        let alert = SCLAlertView()
        alert.addButton("OK") {
            self.submit = true
        }
        alert.showTitle( "Nộp bài", subTitle: "Bạn có chắc chắn muốn nộp bài?" , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    // MARK: - Others
    @objc func LayDiem(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                self.daNopBai = true
                btnSubmit.isHidden = true
                let messageDialog = UIAlertController(title: "Kết quả bài kiểm tra", message: "Điểm của bạn: \(msg as! String)", preferredStyle: .alert)
                messageDialog.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(messageDialog, animated: true, completion: nil)
            }
        }
    }
    
    @objc func LayDiemFail(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                self.daNopBai = true
                btnSubmit.isHidden = true
                let messageDialog = UIAlertController(title: "Lỗi", message: msg as? String, preferredStyle: .alert)
                messageDialog.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(messageDialog, animated: true, completion: nil)
            }
        }
    }
    
    @objc func LayTime(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                blockchainTime = msg as? Int
                self.SubTime()
            }
        }
    }
}

// MARK: - Extensions
extension LamBai: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return choice[sttCauHoi - 1].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellAnswerForStudent") as? CellAnswerForStudent else {
            return UITableViewCell()
        }
        cell.setChoice(choice[sttCauHoi - 1][indexPath.row], setYourChoice[sttCauHoi - 1][indexPath.row])
        tableView.tableFooterView = UIView()
        return cell
    }

}

extension LamBai: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellQuestion") as? CellQuestion else {
            return UITableViewCell()
        }
        cell.setQuestion(question[sttCauHoi - 1], String(sttCauHoi))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let kichthuoc = getSize(question[sttCauHoi - 1])
        if (kichthuoc.width / view.frame.width > 1) {
            return (kichthuoc.width / view.frame.width + 2) * kichthuoc.height + 100
        }
        else {
            return view.frame.height / 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        yourAnswer[sttCauHoi - 1] = indexPath.row
        for i in 0..<choice[sttCauHoi - 1].count {
            if i == indexPath.row {
                setYourChoice[sttCauHoi - 1][i] = true
            } else {
                setYourChoice[sttCauHoi - 1][i] = false
            }
        }
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
}
