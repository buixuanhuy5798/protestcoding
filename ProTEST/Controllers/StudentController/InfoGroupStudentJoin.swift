//
//  InfoGroupStudentJoin.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift
import SCLAlertView
import KeychainSwift
import QRCoder

class InfoGroupStudentJoin: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var QRImage: UIImageView!
    @IBOutlet weak var lblIDNhom: UILabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblIDTeacher: UILabel!
    @IBOutlet weak var lblNumberOfMem: UILabel!
    @IBOutlet weak var btnExitGroup: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewCover: UIView!
    
    // MARK: - Variables
    let keychain = KeychainSwift()
    let task = SocketIOManager.sharedInstance
    var IDNhom: String?
    var teacherName: String?
    var IDTeacher: String?
    var numberOfMem: String?
    var IDUser: String?
    var groupName:String?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfoFromServer()
        loading.isHidden = true
        viewCover.alpha = 0.5
        viewCover.isHidden = true
    }
    
    // MARK: - Setup
    func getInfoFromServer() {
        let generator = QRCodeGenerator()
        QRImage.image = generator.createImage(value: "GR\n" + IDNhom! + "\n" + groupName! + "\n" + numberOfMem!, size: QRImage.frame.size)
        lblIDNhom.text = IDNhom
        lblTeacherName.text = teacherName
        lblIDTeacher.text = IDTeacher
        lblNumberOfMem.text = numberOfMem
    }
    
    // MARK: - Actions
    @IBAction func ExitGroup(_ sender: Any) {
        let alert = SCLAlertView()
        alert.addButton("OK") {
            StudentGroupController.change = true
            self.task.studentExitOrRefuseGroup(self.IDNhom, self.keychain.get("username"), self.viewCover, self.loading)
            self.loading.startAnimating()
            self.loading.isHidden = false
            self.task.rstudentExitOrRefuseGroup(self.loading, self.viewCover)
            NotificationCenter.default.addObserver(self, selector: #selector(self.outGroupS(notifi:)), name: Notification.Name(rawValue:"rstudentExitOrRefuseGroupSuccess"),object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.outGroupF(notifi:)), name: Notification.Name(rawValue:"rstudentExitOrRefuseGroupFail"),object: nil)
        }
        alert.showTitle( "Thoát nhóm ", subTitle: "Có chắc chắn muốn thoát nhóm ?" , timeout: nil, completeText: "Cancel", style: .edit, colorStyle: 0x75C296, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    // MARK: - Others
    @objc func outGroupS(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func outGroupF(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
            }
        }
    }
}
