//
//  InfoGroupStudent.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/17/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class InfoGroupStudentRequest: UIViewController {
    
    // MARK: - Outletss
    @IBOutlet weak var lblIDNhom: UILabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblIDTeacher: UILabel!
    @IBOutlet weak var lblNumberOfMem: UILabel!
    
    // MARK: - Variables
    var IDNhom: String?
    var teacherName: String?
    var IDTeacher: String?
    var numberOfMem: String?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfoFromServer()
    }
    
    // MARK: - Setup
    func getInfoFromServer() {
        lblIDNhom.text = IDNhom
        lblTeacherName.text = teacherName
        lblIDTeacher.text = IDTeacher
        lblNumberOfMem.text = numberOfMem
    }
}
