//
//  RequestJoinGroup.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/22/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder

class RequestJoinGroup: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var QRimage: UIImageView!
    @IBOutlet weak var btnOL: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var lblMember: UILabel!
    @IBOutlet weak var lblIdGV: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblIDgroup: UILabel!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var IDNhom: String?
    var groupName: String?
    var teacherName: String?
    var IDTeacher: String?
    var numberOfMem: String?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getInfoFromServer()
    }
    
    // MARK: - Setup
    func getInfoFromServer() {
        let generator = QRCodeGenerator()
        QRimage.image = generator.createImage(value: "GR\n" + IDNhom! + "\n" + groupName! + "\n" + numberOfMem!, size: QRimage.frame.size)
        lblIDgroup.text = IDNhom
        lblName.text = teacherName
        lblIdGV.text = IDTeacher
        lblMember.text = numberOfMem
    }
    
    // MARK: - Actions
    @IBAction func btnRequest(_ sender: Any) {
        task.studentJoinOrAcceptGroup(lblIDgroup.text,LoginController.user, viewLoading, loading)
        task.rstudentJoinOrAcceptGroup(loading, viewLoading)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inGroupS(notifi:)), name: Notification.Name(rawValue:"rstudentJoinOrAcceptGroupSuccess"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inGroupF(notifi:)), name: Notification.Name(rawValue:"rstudentJoinOrAcceptGroupFail"),object: nil)
    }
    
    // MARK: - Others
    @objc func inGroupS(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                btnOL.setTitle("Đã gửi yêu cầu tham gia nhóm", for: .normal)
                btnOL.backgroundColor = #colorLiteral(red: 0, green: 0.9921568627, blue: 1, alpha: 1)
                let view = UIView()
                view.backgroundColor = UIColor.clear
                btnOL.addSubview(view)
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
             
            }
        }
    }
    
    @objc func inGroupF(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let responseText = msg as! String
                self.view.makeToast(responseText, duration: 2.0, position: .center)
            }
        }
    }
}
