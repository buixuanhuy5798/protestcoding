//
//  PageGroupControllerStudent.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Parchment

class PageGroupControllerStudent: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewCover: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var status: String?
    var IdGroup: String?
    var member: String?
    var MemberGroup = [Member]()
    var Memberinvited = [Member]()
    var MemberRequest = [Member]()
    var MemberDelete = [Member]()// Minh khong cho vao nhom
    var MemberDeny = [Member]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCover.isHidden = false
        activity.isHidden = false
        activity.startAnimating()
        task.rgetInfoOfGroup(activity, viewCover)
        ReceiveGroupSpecific()
    }
    
    // MARK: - Setup
    func createView(_ dataGroup: DataGroupSpecific) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if status == "2" {
            guard let controller1 = storyboard.instantiateViewController(withIdentifier: "InfoGroupStudentRequest") as? InfoGroupStudentRequest else {
                return
            }
            controller1.IDNhom = IdGroup
            controller1.numberOfMem = member
            controller1.teacherName = dataGroup.tname
            controller1.IDTeacher = dataGroup.tuser
            
            let pagingViewController = FixedPagingViewController(viewControllers: [controller1])
            pagingViewController.indicatorColor = #colorLiteral(red: 0.4039215686, green: 0.7411764706, blue: 0.537254902, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.bounds.width, height: 0)
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
            
        } else if status == "1" {
            guard let controller1 = storyboard.instantiateViewController(withIdentifier: "InfoGroupStudentJoin") as? InfoGroupStudentJoin else {
                return
            }
            
            controller1.groupName = dataGroup.gname
            controller1.IDNhom = IdGroup
            controller1.numberOfMem = String(MemberGroup.count)
            controller1.teacherName = dataGroup.tname
            controller1.IDTeacher = dataGroup.tuser
            
            guard let controller2 = storyboard.instantiateViewController(withIdentifier: "MemberStudentController") as? MemberStudentController else {
                return
            }
            controller2.teacherName = dataGroup.tname
            controller2.teacherID = dataGroup.tuser
            controller2.MemberGroup = MemberGroup
            controller2.Memberinvited = Memberinvited
            controller2.MemberRequest = MemberRequest
            print(MemberDeny)
            guard let controller3 = storyboard.instantiateViewController(withIdentifier: "StudentContestController") as? StudentContestController else {
                return
            }
            controller3.idGroup = IdGroup
            let controller4 = storyboard.instantiateViewController(withIdentifier: "NotiGroupTeacher") as! NotiGroupTeacher
            controller4.sectionTitle.removeAll()
            controller4.rowForSection.removeAll()
            if MemberRequest.count > 0 {
                controller4.rowForSection.append(MemberRequest)
                controller4.sectionTitle.append("Đang gửi yêu cầu tham gia nhóm")
            }
            if Memberinvited.count > 0 {
                controller4.rowForSection.append(Memberinvited)
                controller4.sectionTitle.append("Đã mời tham gia nhóm")
            }
            if MemberDeny.count > 0 {
                controller4.rowForSection.append(MemberDeny)
                controller4.sectionTitle.append("Từ chối tham gia nhóm")
            }
            if MemberDelete.count > 0 {
                controller4.rowForSection.append(MemberDelete)
                controller4.sectionTitle.append("Không được chấp nhận vào nhóm")
            }
            print(controller4.rowForSection)
            let pagingViewController = FixedPagingViewController(viewControllers: [
                controller1,
                controller2,
                controller3,
                controller4
                
                ])
            pagingViewController.indicatorColor = #colorLiteral(red: 0.4039215686, green: 0.7411764706, blue: 0.537254902, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.bounds.width/4, height: view.bounds.height/15
            )
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
            
        } else if status == "3" {
            guard let controller1 = storyboard.instantiateViewController(withIdentifier: "InfoGroupStudentAcceptOrReject") as? InfoGroupStudentAcceptOrReject else {
                return
            }
            controller1.IDNhom = IdGroup
            controller1.numberOfMem = String(MemberGroup.count)
            controller1.teacherName = dataGroup.tname
            controller1.IDTeacher = dataGroup.tuser
            controller1.groupName = dataGroup.gname
            let pagingViewController = FixedPagingViewController(viewControllers: [controller1])
            pagingViewController.indicatorColor = #colorLiteral(red: 0.4039215686, green: 0.7411764706, blue: 0.537254902, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.bounds.width, height: 0)
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
        }
        else {
            guard let controller1 = storyboard.instantiateViewController(withIdentifier: "RequestJoinGroup") as? RequestJoinGroup else {
                return
            }
            controller1.groupName = dataGroup.gname
            controller1.IDNhom = IdGroup
            controller1.numberOfMem = member
            controller1.teacherName = dataGroup.tname
            controller1.IDTeacher = dataGroup.tuser
            
            let pagingViewController = FixedPagingViewController(viewControllers: [controller1])
            pagingViewController.indicatorColor = #colorLiteral(red: 0.4039215686, green: 0.7411764706, blue: 0.537254902, alpha: 1)
            pagingViewController.textColor = UIColor.gray
            pagingViewController.selectedTextColor = UIColor.black
            pagingViewController.menuItemSize = .fixed(width: view.bounds.width, height: 0)
            addChild(pagingViewController)
            view.addSubview(pagingViewController.view)
            view.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
        }
    }
    
    // MARK: - Supporting Methods
    func ReceiveGroupSpecific() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroupSpecific(notifi:)), name: Notification.Name(rawValue:"InfoGroupSpecific"),object: nil)
    }
    
    @objc func updateGroupSpecific(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as! DataGroupSpecific
                status = "0"
                for i in temp.arr! {
                    if i.user == LoginController.user {
                        status = i.status
                        break
                    }
                }
                
                 if status == "1"{
                    MemberGroup.removeAll(); Memberinvited.removeAll(); MemberRequest.removeAll()
                    
                    for i in temp.arr! {
                        if i.status == "1" {
                            let x = Member(i.name!, i.user!)
                            MemberGroup.append(x)
                        }
                        else if i.status == "2" {
                            let x = Member(i.name!,i.user!)
                            MemberRequest.append(x)
                        }
                        else if i.status == "3" {
                            let x = Member(i.name!,i.user!)
                            Memberinvited.append(x)
                        }
                        else if i.status == "4" {
                            let x = Member(i.name!,i.user!)
                            MemberDeny.append(x)
                        }
                        else if i.status == "5" {
                            let x = Member(i.name!,i.user!)
                            MemberDelete.append(x)
                        }
                    }
                }
                createView(temp)
            }
        }
    }
}
