//
//  StudentHomeController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QRCoder
import KeychainSwift

class StudentHomeController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewBao: UIView!
    @IBOutlet weak var btnNhom: UIButton!
    @IBOutlet weak var btnChitiet: UIButton!
    @IBOutlet weak var QRImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageStudent: UIImageView!
    @IBOutlet weak var lblStudentName: UILabel!
    @IBOutlet weak var lblStudentPhone: UILabel!
    @IBOutlet weak var numberOfJoinGroup: UILabel!
    @IBOutlet weak var numberOfExam: UILabel!
    @IBOutlet weak var btnDangXuat: UIButton!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    let keychain = KeychainSwift()
    var info: DataLogin?
    var allJoinGroup: ResponseGroup?
    var id: String!
    var duLieuLamDe: DataViewPointStudent?
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDangXuat.layer.cornerRadius = 10
        btnDangXuat.clipsToBounds = true
        setDuLieuStudent()
        btnChitiet.isHidden = true
        task.viewAllPointOfStudent(id, nil, nil)
        task.rviewAllPointOfStudent(nil, nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(LoadData(notifi:)), name: Notification.Name(rawValue:"InfoAllGroupStudentJoin_Change"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(update(notifi:)), name: Notification.Name(rawValue:"rviewAllPointOfStudent"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fail), name: Notification.Name(rawValue:"rviewAllPointOfStudentF"),object: nil)
    }
    
    // MARK: - Setup
    func setDuLieuStudent() {
        viewBao.layer.cornerRadius = 17
        btnNhom.layer.cornerRadius = 10
        btnChitiet.layer.cornerRadius = 10
        
        let generator = QRCodeGenerator()
        QRImage.image = generator.createImage(value: "SV" + id,size: QRImage.frame.size)
        lblName.text = id
        lblStudentName.text = info?.name
        lblStudentPhone.text = info?.phone
        var joinedGroup = 0;
        for i in (allJoinGroup?.data?.arr)! {
            if i.status == "1" || i.status == "3" {
                joinedGroup += 1
            }
        }
        numberOfJoinGroup.text = String(joinedGroup)
    }
    
    // MARK: - Actions
    @IBAction func btnViewPointStudent(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let duLieuLamDe = duLieuLamDe {
            if duLieuLamDe.len != "1" {
                guard let controller = sb.instantiateViewController(withIdentifier: "TabBarThongKe") as? UITabBarController else {
                    return
                }
                
                guard let controllerDiem = controller.viewControllers![0] as? ViewPointStudentController else {
                    return
                }
                
                guard let controllerBieuDo = controller.viewControllers![1] as? ChartViewStudentController else {
                    return
                }
                
                controllerDiem.suser = id
                controllerBieuDo.duLieuTraVe = duLieuLamDe
                self.present(controller, animated: true, completion: nil)
            } else {
                guard let controller = sb.instantiateViewController(withIdentifier: "ViewPointForStudentDoOneExam") as? ViewPointForStudentDoOneExam else {
                    return
                }
                controller.suser = id
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func DetailGroupStudent(_ sender: Any) {
        guard let controller = tabBarController?.viewControllers![2] as? StudentGroupController else {
            return
        }
        tabBarController?.selectedViewController = controller
    }
    
    @IBAction func SignOut(_ sender: Any) {
        SocketIOManager.firstChange = true
        SocketIOManager.firstChange2 = true
        LoginController.user = nil
        keychain.delete("teacher"); keychain.delete("username")
        GroupController.change = false
        InfoGroup.cuatoi = true
        PageGroupController.change = false
        StudentGroupController.change = false
        StudentContestController.timeInListExam = true
        LoginController.teacherLogin = false
        LoginController.studentLogin = false
        task.closeConnection()
        self.dismiss(animated: false, completion: nil)
    }
    
    
    // MARK: - Others
    @objc func LoadData(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                allJoinGroup = msg as? ResponseGroup
                setDuLieuStudent()
            }
        }
    }
    
    @objc func update(notifi: Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? DataViewPointStudent {
                    duLieuLamDe = temp
                    numberOfExam.text = duLieuLamDe?.len
                    if numberOfExam.text != "0" {
                        btnChitiet.isHidden = false
                    }
                }
            }
        }
    }
    
    @objc func fail() {
        self.view.makeToast("Lấy thông tin đề thi thất bại", duration: 2.5, position: .bottom)
    }
}
