//
//  ChartViewStudentController.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 12/12/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import QuartzCore

class ChartViewStudentController: UIViewController, LineChartDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var lblHang: UILabel!
    @IBOutlet weak var lblCot: UILabel!
    
    
    // MARK: - Variables
    var label = UILabel()
    var lineChart: LineChart!
    var duLieuTraVe: DataViewPointStudent?
    // simple arrays
    // simple line with custom x axis labels
    var data = [CGFloat]()
    var xLabels = [String]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblHang.text = "Trục ⏤ là mã các đề thi đã làm"
        lblCot.text = "Trục⎪là điểm số"
        let screenHeight = UIScreen.main.bounds.size.height * 0.6
        
        var views: [String: AnyObject] = [:]
        
        label.text = "..."
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        self.view.addSubview(label)
        views["label"] = label
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-120-[label]", options: [], metrics: nil, views: views))
        
        if Int((duLieuTraVe?.len)!)! != 1 {
            for i in 0..<Int((duLieuTraVe?.len)!)! {
                let t1 = (duLieuTraVe?.arr![i].point)!
                let t2 = (duLieuTraVe?.arr![i].eid)!
                data.append(CGFloat(Double(t1)!) * 10)
                xLabels.append(t2)
            }
        } else {
            let t1 = (duLieuTraVe?.arr![0].point)!
            let t2 = (duLieuTraVe?.arr![0].eid)!
            data.append(CGFloat(Double(t1)!))
            xLabels.append(t2)
            print("TH1")
        }
        
        lineChart = LineChart()
        lineChart.animation.enabled = true
        lineChart.area = true
        lineChart.x.labels.visible = true
        lineChart.x.grid.count = 5
        lineChart.y.grid.count = 5
        lineChart.x.labels.values = xLabels
        lineChart.y.labels.visible = true
        lineChart.addLine(data)
        
        lineChart.translatesAutoresizingMaskIntoConstraints = false
        lineChart.delegate = self
        self.view.addSubview(lineChart)
        views["chart"] = lineChart
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[chart]-|", options: [], metrics: nil, views: views))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[label]-[chart(==\(screenHeight))]", options: [], metrics: nil, views: views))
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     * Line chart delegate method.
     */
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
        label.text = "điểm thi của bạn là \(yValues.first!) với mã đề thi \(xLabels[Int(x)])"
    }
    
    
    
    /**
     * Redraw chart on device rotation.
     */
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if let chart = lineChart {
            chart.setNeedsDisplay()
        }
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
