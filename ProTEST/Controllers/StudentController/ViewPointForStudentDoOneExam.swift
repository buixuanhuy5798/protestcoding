//
//  ViewPointForStudentDoOneExam.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 12/13/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ViewPointForStudentDoOneExam: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tableViewResult: UITableView!
    @IBOutlet weak var viewLoading: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var suser: String!
    var lenArr: String?
    var DataViewPoint = [ArrViewPointStudent]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        task.viewAllPointOfStudent(suser, loading, viewLoading)
        task.rviewAllPointOfStudent(loading, viewLoading)
        ReceiveViewPoint()
    }
    
    func setUp(){
        tableViewResult.delegate = self
        tableViewResult.dataSource = self
        tableViewResult.register(UINib(nibName: "CellViewPoint", bundle: nil), forCellReuseIdentifier: "CellViewPoint")
    }
    
    func ReceiveViewPoint() {
        NotificationCenter.default.addObserver(self, selector: #selector(update(notifi:)), name: Notification.Name(rawValue:"rviewAllPointOfStudent"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fail), name: Notification.Name(rawValue:"rviewAllPointOfStudentF"),object: nil)
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func update(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? DataViewPointStudent{
                    lenArr = temp.len
                    DataViewPoint = temp.arr!
                    self.view.makeToast("Chức năng biểu đồ không áp dụng với số bài đã làm ít hơn 2", duration: 2.5, position: .center)
                } else{
                    print("LOI")
                }
            }
        }
        tableViewResult.reloadData()
        setUp()
    }
    
    @objc func fail(){
        self.view.makeToast("Thao tác thất bại", duration: 3, position: .bottom)
    }

}

extension ViewPointForStudentDoOneExam: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(lenArr!)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellViewPoint") as? CellViewPoint else {
            return UITableViewCell()
        }
        cell.set("Exam id: " + DataViewPoint[indexPath.row].eid!, "Đúng: " + DataViewPoint[indexPath.row].point! + "/" + DataViewPoint[indexPath.row].qlen!)
        return cell
    }
    
}

extension ViewPointForStudentDoOneExam: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.height / 12
    }
}
