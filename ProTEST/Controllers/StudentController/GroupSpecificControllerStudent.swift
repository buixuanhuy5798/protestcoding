//
//  GroupSpecificControllerStudent.swift
//  ProTEST
//
//  Created by Đỗ Hồng Quân on 11/14/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Parchment

class GroupSpecificControllerStudent: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var UIVIEW: UIView!
    @IBOutlet weak var lblGroupName: UILabel!
    
    // MARK: - Variables
    var groupName: String?
    var status: String?
    var IdGroup: String?
    var member: String?

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblGroupName.text = groupName
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PageGroupControllerStudent") as? PageGroupControllerStudent else {
            return
        }
        viewController.status = status
        viewController.IdGroup = IdGroup
        viewController.member = member
        viewController.view.frame = CGRect(x: 0, y: 0, width: UIVIEW.frame.width, height: UIVIEW.frame.height)
        self.addChild(viewController)
        UIVIEW.addSubview(viewController.view)
    }
    
    // MARK: - Actions
    @IBAction func Action_Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
