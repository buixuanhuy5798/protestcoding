//
//  ViewPointStudentController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/28/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class ViewPointStudentController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var viewloading: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var tbvResult: UITableView!
    
    // MARK: - Variables
    let task = SocketIOManager.sharedInstance
    var suser: String!
    var lenArr: String?
    var DataViewPoint = [ArrViewPointStudent]()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        task.viewAllPointOfStudent(suser, loading, viewloading)
        task.rviewAllPointOfStudent(loading, viewloading)
        ReceiveViewPoint()
    }
    
    // MARK: - Setup
    func setUp(){
        tbvResult.delegate = self
        tbvResult.dataSource = self
        tbvResult.register(UINib(nibName: "CellViewPoint", bundle: nil), forCellReuseIdentifier: "CellViewPoint")
    }
    
    // MARK: - Supporting Methods
    func ReceiveViewPoint() {
        NotificationCenter.default.addObserver(self, selector: #selector(update(notifi:)), name: Notification.Name(rawValue:"rviewAllPointOfStudent"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fail), name: Notification.Name(rawValue:"rviewAllPointOfStudentF"),object: nil)
    }
    
    @objc func update(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                if let temp = msg as? DataViewPointStudent{
                    lenArr = temp.len
                    DataViewPoint = temp.arr!
                } else{
                    print("LOI")
                }
            }
        }
        tbvResult.reloadData()
        setUp()
    }
    
    @objc func fail(){
        self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Extensions
extension ViewPointStudentController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(lenArr!)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellViewPoint") as? CellViewPoint else {
            return UITableViewCell()
        }
        cell.set("Exam id: " + DataViewPoint[indexPath.row].eid!, "Đúng: " + DataViewPoint[indexPath.row].point! + "/" + DataViewPoint[indexPath.row].qlen!)
        return cell
    }
    
}

extension ViewPointStudentController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.height / 12
    }
}
