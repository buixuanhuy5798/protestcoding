//
//  SearchInfo.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/24/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import KeychainSwift

class SearchInfoTeacher: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var btnEXOL: UIButton!
    @IBOutlet weak var btnOL: UIButton!
    @IBOutlet weak var lblNameHeader: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    
    // MARK: - Variables
    var gv = true
    var ID:String?
    var name:String?
    var phone:String?
    let a = KeychainSwift()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUP()
    }
    
    // MARK: - Setup
    func setUP(){
        if (gv){
            if a.getBool("teacher")!{
                btnOL.isHidden = true
                btnEXOL.isHidden = true
            }
            btnOL.setTitle("Nhóm quản lý", for: .normal)
        }
        else {
            if a.getBool("teacher")!{
                btnOL.isHidden = true
            }
            btnEXOL.isHidden = true
            btnOL.setTitle("Nhóm tham gia", for: .normal)
        }
        lblID.text = ID
        lblName.text = name
        lblNameHeader.text = name
        lblPhone.text = phone
    }
    
    // MARK: - Actions
    @IBAction func btnExam(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = sb.instantiateViewController(withIdentifier: "PublicExam") as? PublicExam else {
            return
        }
        controller.tuser =  ID
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnGroup(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = sb.instantiateViewController(withIdentifier: "GroupSearchInfo") as? GroupSearchInfo else {
            return
        }
        if(gv){
            controller.gv = true
            controller.id = ID
            self.present(controller, animated: true, completion: nil)
        }
        else {
            controller.gv = false
            controller.id = ID
            self.present(controller, animated: true, completion: nil)
        }
    }
}
