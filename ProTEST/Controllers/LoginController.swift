//
//  LoginController.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 10/24/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Toast_Swift
import KeychainSwift

class LoginController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var viewAccount: UIView!
    @IBOutlet weak var btnLoginOL: UIButton!
    @IBOutlet weak var viewPass: UIView!
    @IBOutlet weak var lblNoti: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblErrorID: UILabel!
    @IBOutlet weak var lblErrorPass: UILabel!
    
    // MARK: - Variables
    var task = SocketIOManager.sharedInstance
    var info: DataLogin?
    let keychain = KeychainSwift()
    static var user:String?
    static var signedout = false
    static var teacherLogin = false
    static var studentLogin = false
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        activityIndicator.isHidden = true
        customVIEW()
        setUplblError()
        ReceiveLoginNoti()
        ReceiveGroupNoti()
    }
    
    // MARK: - Actions
    @IBAction func btnLogin(_ sender: Any) {
        setUplblError()
        if (tfUsername.text?.isEmpty)! {
            lblErrorID.isHidden = false
            lblErrorID.text = "Tên tài khoản bị trống"
        } else if (tfPassword.text?.isEmpty)! {
            lblErrorPass.isHidden = false
            lblErrorPass.text = "Mật khẩu bị trống"
        } else {
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            if Reachability.isConnectedToNetwork() {
                task.establishConnection {[weak self] in
                    self?.task.DangNhap(self?.tfUsername.text, self?.tfPassword.text)
                    self?.task.DNThanhCong(self?.activityIndicator)
                }
            } else {
                self.view.makeToast("Không thể kết nối Internet.\n Vui lòng kiểm tra lại kết nối", duration: 3.0, position: .center)
                activityIndicator.isHidden = true
            }
        }
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "RegisterController") as? RegisterController else {
            return
        }
        navigationController?.navigationBar.isHidden = false
        navigationController?.pushViewController(controller, animated: true)
        
    }
    
    // MARK: - Setup
    func customVIEW() {
        viewAccount.layer.cornerRadius = 12
        viewPass.layer.cornerRadius = 12
        btnLoginOL.layer.cornerRadius = 12
    }
    
    func setUplblError() {
        lblErrorID.isHidden = true
        lblErrorPass.isHidden = true
    }
    
    // MARK: - Functions
    func ReceiveLoginNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(listen(notifi:)), name: Notification.Name(rawValue:"LoginNoti"),object: nil)
        if !LoginController.signedout {
            NotificationCenter.default.addObserver(self, selector: #selector(updatenoti(notifi:)), name: Notification.Name(rawValue:"LoginSuccess"),object: nil)
            LoginController.signedout = true
        }
    }
    
    func ReceiveGroupNoti() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateGroup(notifi:)), name: Notification.Name(rawValue:"InfoGroupTeacherManage"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateInfoGroup(notifi:)), name: Notification.Name(rawValue:"InfoAllGroupStudentJoin"),object: nil)
    }

    
    @objc func listen(notifi : Notification){
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                lblNoti.text = msg as? String
                lblNoti.isHidden = false
            }
        }
    }
    
    @objc func updatenoti(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? ResponseLogin
                lblNoti.isHidden = true
                if temp?.msg == "teacher" {
                    self.task.getInfoAllGroupTeacherManage(tfUsername.text!, self.activityIndicator)
                    self.task.rgetInfoAllGroupTeacherMange(self.activityIndicator)
                    
                } else {
                    self.task.getInfoAllGroupStudentJoin(tfUsername.text!, self.activityIndicator)
                    self.task.rgetInfoAllGroupStudentJoin(self.activityIndicator)
                }
                LoginController.user = tfUsername.text
                info = temp?.data
            }
        }
    }
    
    @objc func updateGroup(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? DataGroup
                let sb = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = sb.instantiateViewController(withIdentifier: "TabBarController_Teacher") as? UITabBarController else {
                    return
                }
                let controller0 = controller.viewControllers![0] as! TeacherHomeController
                controller0.info = info
               
                controller0.groupDT = temp
                controller0.id = tfUsername.text
                let controller1 = controller.viewControllers![1] as! GroupController
                controller1.dataGroup = temp
                
                controller1.id = tfUsername.text
                keychain.set(true, forKey: "teacher")
                keychain.set(tfUsername.text!, forKey: "username")
                if !LoginController.teacherLogin {
                    present(controller, animated: false, completion: nil)
                    LoginController.teacherLogin = true
                }
                
            }
        }
    }
    
    @objc func updateInfoGroup(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                let temp = msg as? ResponseGroup
                let sb = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = sb.instantiateViewController(withIdentifier: "TabBarController_Student") as? UITabBarController else {
                    return
                }
                
                let controller0 = controller.viewControllers![0] as! StudentHomeController
                controller0.info = info
                controller0.allJoinGroup = temp
                controller0.id = tfUsername.text
                let controller2 = controller.viewControllers![2] as! StudentGroupController
                controller2.allGroup = temp
                keychain.set(false, forKey: "teacher")
                keychain.set(tfUsername.text!, forKey: "username")
                if !LoginController.studentLogin {
                    present(controller, animated: false, completion: nil)
                    LoginController.studentLogin = true
                }
            }
        }
    }
    
}
