//
//  QR.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 11/24/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit

class QR: UIViewController {

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
