//
//  Chart1ExamViewPoint.swift
//  ProTEST
//
//  Created by Bùi Xuân Huy on 12/10/18.
//  Copyright © 2018 huy. All rights reserved.
//

import UIKit
import Charts
import Toast_Swift
class Chart1ExamViewPoint: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var lblDoKho: UILabel!
    @IBOutlet weak var lblDuoiTB: UILabel!
    @IBOutlet weak var lblTrenTB: UILabel!
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var viewloading: UIView!
    
    
    // MARK: -Variables
    var eid:String!
    var tong = 0
    let task = SocketIOManager.sharedInstance
    var DataViewPoint = [ArrViewPoint]()
    var qlen:Double?
    var trenTB = 0
    var duoiTB = 0
    var unitsSold = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ReceiveViewPoint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         task.viewAllPointOfExam(eid, loading,viewloading)
        task.rviewAllPointOfExam(loading, viewloading)
    }
    
    // MARK: - Supporting Methods
    func ReceiveViewPoint() {
        NotificationCenter.default.addObserver(self, selector: #selector(update(notifi:)), name: Notification.Name(rawValue:"rviewAllPointOfExam"),object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(fail), name: Notification.Name(rawValue:"rviewAllPointOfExamF"),object: nil)
    }
    
    @objc func update(notifi : Notification) {
        if let mess = notifi.userInfo {
            if let msg = mess["message"] {
                
                for i in 0..<unitsSold.count{
                    unitsSold[i] = 0
                }
                
                if let temp = msg as? DataViewPoint{
                    if temp.len == "0" {
                        barChart.noDataText = "Chưa có thông tin"
                        barChart.noDataFont = UIFont(name: "System", size: 20)
                        self.view.makeToast("Chưa có sinh viên nào làm bài kiểm tra này", duration: 1.0, position: .center)
                        lblDoKho.text = ""
                        lblTrenTB.text = ""
                        lblDuoiTB.text = ""
                    } else
                    {
                        DataViewPoint = temp.arr!
                        qlen = Double(temp.qlen!)
                    
                        for i in DataViewPoint {
                            
                            let x = Int(Double(i.point!)!/qlen!*10)
                            tong = tong + x
                            if (x>=5) {
                                trenTB = trenTB+1
                            }
                            else {
                                duoiTB = duoiTB + 1
                            }
                            unitsSold[x] = unitsSold[x]+1
                        }
                        if Double(tong)/qlen! > 8 {
                            lblDoKho.text = "VỪA SỨC"
                            lblDoKho.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                        }
                        else if Double(tong)/qlen! < 5 {
                            lblDoKho.text = "RẤT KHÓ"
                            lblDoKho.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
                        }
                        else {
                            lblDoKho.text = "KHÓ"
                            lblDoKho.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
                        }
                        lblDuoiTB.text = String(duoiTB)
                        lblTrenTB.text = String(trenTB)
                        setChart(unitsSold)
                }
            } else
              {
                print("LOI")
              }
                
            }
        }
        
    }
    
    @objc func fail(){
        self.view.makeToast("Thao tác thất bại", duration: 2.5, position: .bottom)
    }
    // MARK: - Actions
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    func setChart(_ values: [Double]) {
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<11 {
            let dataEntry = BarChartDataEntry(x: Double(i), yValues: [values [i]])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Điểm thi")
        chartDataSet.setColor(#colorLiteral(red: 0.3564725816, green: 0.782137692, blue: 0.585753262, alpha: 1))
        
        let chartDa = BarChartData(dataSet: chartDataSet)
        
        barChart.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
        barChart.doubleTapToZoomEnabled = false
        barChart.data = chartDa
      
    
    }
}

